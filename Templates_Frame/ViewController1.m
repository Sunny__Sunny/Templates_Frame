//
//  ViewController1.m
//  SMCustomeTabBar
//
//  Created by Sunny on 16/8/20.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "ViewController1.h"

@interface ViewController1 ()

@end

@implementation ViewController1

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor coffeeColor];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:@"1" forKey:@"type"];
    [param setObject:@"3" forKey:@"version"];
    
    
    [param setObject:@"17601613846" forKey:@"phone"];
    [param setObject:@"123456" forKey:@"password"];
    
    [SMAFNetworkManager postReqeustWithURL:@"/account/login" params:param requestId:@"" serverType:SMNetServer_Base successBlock:^(NSURLSessionDataTask *task, SMAFNetworkResponse *response) {
        if (response.isSuccess) {
            [MBProgressHUD showWithTitle:@"登陆成功"];
        }
    } failureBlock:^(NSURLSessionDataTask *task, SMAFNetworkResponse *response, NSError *error) {
            [MBProgressHUD showWithTitle:@"登陆失败"];
    } showHUD:YES];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
