//
//  AppDelegate+UMeng.m
//  
//
//  Created by Sunny on 16/7/8.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "AppDelegate+UMeng.h"

@implementation AppDelegate (UMeng)


- (void)umengApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    NSString *umangKey = nil;
    NSString *bundleID = nil;
    // ------集成友盟
    // ------打印友盟数据统计信息 ！！！！！上线时要删除或者注销！！！！！！！
#if DEBUG
    [MobClick setLogEnabled:YES];
//    [UMSocialData openLog:YES];
    umangKey = @"57bc0959e0f55ad4de000760";
    bundleID = @"com.zonetry.platform";
#else
    [MobClick setLogEnabled:NO];
//    [UMSocialData openLog:NO];
    umangKey = @"57bc0959e0f55ad4de000760";
    bundleID = @"com.zonetry.platform";
#endif
    
    // 统计
    [self setupUMengClickWithBundleId:bundleID umanKey:umangKey];
    // 分享
    [self setupUMengShareBundleId:bundleID umanKey:umangKey];
}
/**
 *  友盟统计
 *
 *  @param bundleID appId
 *  @param umanKey  友盟key
 */
-(void)setupUMengClickWithBundleId:(NSString *)bundleID umanKey:(NSString *)umanKey{

    NSString *appBundleID = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    if ([appBundleID isEqualToString:bundleID]) {
        
        UMConfigInstance.appKey = umanKey;
        UMConfigInstance.channelId = @"App Store";
        UMConfigInstance.ePolicy = BATCH;
        // 友盟统计
        [MobClick startWithConfigure:UMConfigInstance];
         // 设置版本号
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        [MobClick setAppVersion:version];
    }
}
/**
 *  友盟分享
 *
 *  @param bundleID appId
 *  @param umanKey  友盟key
 */
- (void) setupUMengShareBundleId:(NSString *)bundleID umanKey:(NSString *)umanKey{

//    //设置友盟社会化组件appkey
//    [UMSocialData setAppKey:umanKey];
//    //设置微信AppId、appSecret，分享url
//    [UMSocialWechatHandler setWXAppId:@"wx05464e77d0bf678c"
//                            appSecret:@"b38fb8aee151ac8ea59a7c68d7531df2"
//                                  url:@"http://www.umeng.com/social"];
//    //设置手机QQ 的AppId，Appkey，和分享URL，需要#import "UMSocialQQHandler.h"
//    [UMSocialQQHandler setQQWithAppId:@"1105638292"
//                               appKey:@"vRlpw1dNJT9OQmb5"
//                                  url:@"http://www.umeng.com/social"];
//    //打开新浪微博的SSO开关，设置新浪微博回调地址，这里必须要和你在新浪微博后台设置的回调地址一致。需要 #import "UMSocialSinaSSOHandler.h"
//    // 新浪得在第一版上线以后才能通关审核  需要填写第一版的下载地址
////    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:@"3637468945"
////                                              secret:@"b130a0aa0c68493d41fd6eccf2d12bde"
////                                         RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    
}

// 开始记录页面停留的时间
+ (void)beginLogPageView:(__unsafe_unretained Class)pageView {
    [MobClick beginLogPageView:NSStringFromClass(pageView)];
}
// 结束记录页面停留的时间
+ (void)endLogPageView:(__unsafe_unretained Class)pageView {
    [MobClick endLogPageView:NSStringFromClass(pageView)];
}

// 事件统计统计发生的次数
+ (void)event:(NSString *)eventID {
    [MobClick event:eventID];
}

// 统计统计点击行为各属性被触发的次数
+ (void)event:(NSString *)eventID attributes:(NSDictionary *)attributes {
    [MobClick event:eventID attributes:attributes];
}

@end
