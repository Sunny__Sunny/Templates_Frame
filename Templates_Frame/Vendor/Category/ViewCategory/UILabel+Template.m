
//
//  UILabel+Template.m
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "UILabel+Template.h"

@implementation UILabel (Template)

+ (UILabel *)labelWithFrame:(CGRect)frame
                       text:(NSString *)text
                  textColor:(UIColor *)textColor
                       font:(UIFont *)font
              textAlignment:(NSTextAlignment)textAlignment
                        tag:(NSInteger)tag
                  hasShadow:(BOOL)hasShadow {
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.textColor = textColor;
    label.backgroundColor = [UIColor clearColor];
    if( hasShadow ){
        label.shadowColor = [UIColor blackColor];
        label.shadowOffset = CGSizeMake(1,1);
    }
    label.textAlignment = textAlignment;
    label.font = font;
    if (tag) {
        label.tag = tag;
    }
    return label;
}

- (void)contentSafeString:(NSString*)text
{
    if ([self.text containsString:@"(null)"]) {
        self.text = [text stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    }
    if ([self.text containsString:@"null"]) {
        self.text = [text stringByReplacingOccurrencesOfString:@"null" withString:@""];
    }
    if ([self.text containsString:@"<null>"]) {
        self.text = [text stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
    }
}
@end
