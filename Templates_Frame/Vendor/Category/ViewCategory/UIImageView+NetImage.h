//
//  UIImageView+NetImage.h
//  zoneTry
//
//  Created by Sunny on 16/8/11.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (NetImage)

typedef void (^tapGestureBlock)(UIImageView *imageView);
/**
 *  以中心点截取图片
 *
 *  @param image
 *  @param asize
 *
 *  @return
 */
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize;

+ (void)startImageViewRotateAnimation:(UIImageView *)currentImg forKey:(NSString*)key;

+ (void)stopImageViewRotateAnimation:(UIImageView *)currentImg forKey:(NSString*)key;

/*
 [imgView addTapGesture:^(UIImageView *imageView) {
 NSLog(@"imageView tap");
 }];
 */
- (void)addTapGesture:(tapGestureBlock)tapBlock;


/**
 *  加载头像网络图片    使用了圆角ImageView
 *
 *  @param imageUrl   网络地址
 *  @param placeImage 默认图
 */
- (void) setNetHeadImage:(NSString *) imageUrl;
/**
 *  加载正方形网络图片
 *
 *  @param imageUrl   网络地址
 *  @param placeImage 默认图
 */
- (void) setNetContentImage:(NSString *) imageUrl;
/**
 *  加载长方形网络图片
 *
 *  @param imageUrl   网络地址
 *  @param placeImage 默认图
 */
- (void) setNetBigImage:(NSString *) imageUrl;


@end
