//
//  UIViewController+TabbarHiddenWhenPush.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TabbarHiddenWhenPush)

/**
 *  在含有TabBar的情况下 VC之间的跳转
 *
 *  @param sender VC
 */
- (void)tabbarHiddenWhenPushMethod:(UIViewController *)sender;

/**
 *  hud 扩展
 *
 *  @param view
 *  @param hint
 */
- (void)showHudInView:(UIView *)view hint:(NSString *)hint;

- (void)hideHud;

- (void)showHint:(NSString *)hint;

// 从默认(showHint:)显示的位置再往上(下)yOffset
- (void)showHint:(NSString *)hint yOffset:(float)yOffset;

@end
