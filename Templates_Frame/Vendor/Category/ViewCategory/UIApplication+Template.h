//
//  UIApplication+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Template)

/**
 *  当前活动控制器
 *
 *  @return 
 */
- (UIViewController *)activityViewController ;

@end
