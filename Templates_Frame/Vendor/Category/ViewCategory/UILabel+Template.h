//
//  UILabel+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Template)

/**
 *  创建lab
 *
 *  @param frame     frame
 *  @param text      内容
 *  @param textColor text颜色
 *  @param font      字体大小
 *  @param tag       tag值
 *  @param hasShadow 是否要阴影
 *
 *  @return 
 */
+ (UILabel *)labelWithFrame:(CGRect)frame
                       text:(NSString *)text
                  textColor:(UIColor *)textColor
                       font:(UIFont *)font
              textAlignment:(NSTextAlignment)textAlignment
                        tag:(NSInteger)tag
                  hasShadow:(BOOL)hasShadow;
/**
 *  安全的设置text
 *
 *  @param text 
 */
- (void)contentSafeString:(NSString*)text;
@end
