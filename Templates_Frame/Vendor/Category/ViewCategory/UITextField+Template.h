//
//  UITextField+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Template)

/**
 *  获取光标的位置
 *
 *  @return
 */
- (NSRange) selectedRange;
/**
 *  设置光标的位置
 *
 *  @param range 
 */
- (void) setSelectedRange:(NSRange) range;
@end
