//
//  UIImage+RoundCorner.h
//  zoneTry
//
//  Created by Sunny on 16/7/19.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RoundCorner)

- (UIImage *)imageWithRoundedCornersAndSize:(CGSize)sizeToFit andCornerRadius:(CGFloat)radius;

- (UIImage*)transformWidth:(CGFloat)width
                    height:(CGFloat)height;

- (UIImage *)imageFromImage:(UIImage *)image inRect:(CGRect)rect ;

- (UIImage *) getScroImage:(UIImage *)image ;

- (UIImage *) imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth;

@end
