//
//  MBProgressHUD+Template.m
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "MBProgressHUD+Template.h"

@implementation MBProgressHUD (Template)


#define kWindown [UIApplication sharedApplication].keyWindow

static BOOL stop;
static MBProgressHUD *g_hudToast = nil;
static MBProgressHUD *getToast()
{
    static dispatch_once_t onceInIt;
    
    dispatch_once(&onceInIt, ^{
        g_hudToast = [[MBProgressHUD alloc] initWithFrame:CGRectMake(100, 200, 230, 200)];
    });
    g_hudToast.center = CGPointMake(kWindown.frame.size.width/2.0f, kWindown.frame.size.height/2.0f-20);
    [kWindown addSubview:g_hudToast];
    return g_hudToast;
}

#pragma mark 显示信息
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
    // 设置图片
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]]];
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1秒之后再消失
    [hud hide:YES afterDelay:0.7];
}

#pragma mark 显示错误信息
+ (void)showError:(NSString *)error toView:(UIView *)view{
    [self show:error icon:@"error.png" view:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view
{
    [self show:success icon:@"success.png" view:view];
}

/**
 *  纯文字提示,2秒后自动消失
 *
 *  @param title 提示内容
 */
+(void)showWithTitle:(NSString *)title
{
    MBProgressHUD *hud=[MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.margin=5;
    hud.cornerRadius=4.0;
    hud.mode=MBProgressHUDModeCustomView;
    /******自定义标题,可以自动换行*******/
    UILabel *titleLabel=[[UILabel alloc] init];
    titleLabel.numberOfLines=0;
    titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    titleLabel.font=[UIFont systemFontOfSize:15.0f];
    titleLabel.text=title;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.textColor=[UIColor whiteColor];
    CGSize size=[title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]}];
    if (size.width>=kScreenWidth) {
        size.width=kScreenWidth;
    }
    CGRect rect=[title boundingRectWithSize:CGSizeMake(size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]} context:nil];
    titleLabel.frame=CGRectMake(0, 0, size.width, rect.size.height);
    hud.customView=titleLabel;
    [hud hide:YES afterDelay:1.5f];
}

#pragma mark 显示一些信息
+ (MBProgressHUD *)showMessag:(NSString *)message toView:(UIView *)view isSuccess:(BOOL)success{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // YES代表需要蒙版效果
    hud.dimBackground = NO;
    hud.mode = MBProgressHUDModeCustomView;
    UIImageView *customView = [[UIImageView alloc] init];
    if (success) {
        customView.image = [UIImage imageNamed:@"loadingSuccess"];
    } else
    {
        customView.image = [UIImage imageNamed:@"loser"];
    }
    customView.backgroundColor = [UIColor clearColor];
    customView.frame =  CGRectMake(0, 0, 40, 40);
    hud.customView = customView;
    [hud hide:YES afterDelay:1.0f];
    return hud;
}


#pragma mark 显示一些信息
+ (void)showActiveMessageNew:(NSString *)message
{
    MBProgressHUD *HUD = getToast();
    HUD.color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    HUD.mode = MBProgressHUDModeCustomView;
    [HUD setHidden:NO];
    UIImageView *view = [[UIImageView alloc] init];
    view.image = [UIImage imageNamed:@"loading"];
    view.backgroundColor = [UIColor clearColor];
    view.frame =  CGRectMake(0, 0, 40, 40);
    [MBProgressHUD rotateView:view];
    stop = NO;
    HUD.customView = view;
    
    HUD.dimBackground = NO;//显示遮挡板
    HUD.labelText = message;
    [HUD show:YES];
}



+ (void)rotateView:(UIView *)view
{
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^(){
        view.transform = CGAffineTransformRotate(view.transform,M_PI);
    } completion:^(BOOL finish){
        if (stop == YES) {
            return;
        }
        [self rotateView:view];
    }];
}
//进行请求时加载的HUD
+ (void)showActiveMessageNew:(NSString *)message withBackground:(BOOL)backG
{
    MBProgressHUD *HUD = getToast();
    HUD.color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    HUD.mode = MBProgressHUDModeCustomView;
    [HUD setHidden:NO];
    UIImageView *view = [[UIImageView alloc] init];
    view.image = [UIImage imageNamed:@"loading"];
    view.backgroundColor = [UIColor clearColor];
    view.frame =  CGRectMake(0, 0, 40, 40);
    [MBProgressHUD rotateView:view];
    stop = NO;
    HUD.customView = view;
    
    //    HUD.dimBackground = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    HUD.dimBackground = NO;//显示遮挡板
    HUD.labelText = message;
    [HUD show:YES];
    //    [HUD hide:YES afterDelay:1.0f];//指示器转动多长时间消失
}

+(void)dismissHUD
{
    MBProgressHUD *HUD = getToast();
    HUD.hidden = YES;
    stop = YES;
}

//请求失败加载的HUD
+ (void)showActiveMessage:(NSString *)message withBackground:(BOOL)backG
{
    MBProgressHUD *HUD = getToast();
    HUD.mode = MBProgressHUDModeCustomView;
    [HUD setHidden:NO];
    UIImageView *view = [[UIImageView alloc] init];
    if (backG) {
        view.image = [UIImage imageNamed:@"loadingSuccess"];
    } else
    {
        view.image = [UIImage imageNamed:@"loser"];
    }
    
    view.backgroundColor = [UIColor clearColor];
    view.frame =  CGRectMake(0, 0, 40, 40);
    
    HUD.customView = view;
    HUD.removeFromSuperViewOnHide  = YES;
    HUD.dimBackground = NO;//显示遮挡板
    HUD.labelText = message;
    [HUD show:YES];
    [HUD hide:YES afterDelay:1.5f];//指示器转动多长时间消失
}

@end
