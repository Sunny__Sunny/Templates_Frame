//
//  UIView+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Template)

/**
 *  移动到某个点
 *
 *  @param delta 具体点
 */
- (void) moveBy: (CGPoint) delta;
/**
 *  缩小放大
 *
 *  @param scaleFactor 放大缩小倍数
 */
- (void) scaleBy: (CGFloat) scaleFactor;
/**
 *  获取试图控制器
 *
 *  @return 返回控制器
 */
- (UIViewController *) ViewController;
/**
 * Finds the first descendant view (including this view) that is a member of a particular class.
 */
- (UIView*)descendantOrSelfWithClass:(Class)cls;

/**
 * Finds the first ancestor view (including this view) that is a member of a particular class.
 */
- (UIView*)ancestorOrSelfWithClass:(Class)cls;

/**
 * Calculates the offset of this view from another view in screen coordinates.
 *
 * otherView should be a parent view of this view.
 */
- (CGPoint)offsetFromView:(UIView* )otherView;
/**
 *  设置边框
 *
 *  @param borderWidth 边框宽度
 *  @param borderColor 边框颜色
 */
-(void)setBorderWidth:(NSInteger)borderWidth andBorderColor:(UIColor *)borderColor;
/**
 *  设置为圆形
 */
-(void) makeCircleView;
/**
 *  设置圆角
 *
 *  @param radius
 */
-(void)makeCornerRadius:(CGFloat)radius;

@end
