//
//  MBProgressHUD+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (Template)
/**
 *  纯文字提示,2秒后自动消失
 *
 *  @param title 提示内容
 */
+(void)showWithTitle:(NSString *)title;
/**
 *  请求失败提示
 *
 *  @param error
 *  @param view
 */
+ (void)showError:(NSString *)error toView:(UIView *)view;
/**
 *  请求成功提示
 *
 *  @param error
 *  @param view
 */
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;
+ (MBProgressHUD *)showMessag:(NSString *)message toView:(UIView *)view isSuccess:(BOOL)success;
//+ (MBProgressHUD *)showMessag:(NSString *)message toView:(UIView *)view;
#pragma mark 显示一些信息
+ (void)showActiveMessageNew:(NSString *)message;
+ (void)showActiveMessage:(NSString *)message withBackground:(BOOL)backG;
+ (void)showActiveMessageNew:(NSString *)message withBackground:(BOOL)backG;
+ (void)dismissHUD;

@end
