//
//  NSObject+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Template)

/**
 *   字典转换成json字符串
 *
 *  @param data 基本数据类型
 *
 *  @return 字符串
 */
+ (NSString *)toStringWithData:(id)data;

@end
