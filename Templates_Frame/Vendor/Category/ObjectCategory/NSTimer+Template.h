//
//  NSTimer+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (Template)
/**
 *  暂停计时器
 */
- (void)pauseTimer;
/**
 *  重启计时器
 */
- (void)resumeTimer;
/**
 *  开始计时器
 *
 *  @param interval 多少秒之后
 */
- (void)resumeTimerAfterTimeInterval:(NSTimeInterval)interval;

@end
