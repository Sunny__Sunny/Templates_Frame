//
//  NSObject+Template.m
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "NSObject+Template.h"

@implementation NSObject (Template)

+ (NSString *)toStringWithData:(id)data
{
    if (data) {
        NSError *error;
        NSData *dataObj = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
        
        NSString *jsonString = [[NSString alloc] initWithData:dataObj encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    return nil;
}

@end
