//
//  NSString+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Template)

//-----------------------------------------------------------------------------------
// 字符串操作集合，包括
// 1)去掉空格
// 2)判断开头或结尾
// 3)判断是否为空
//-----------------------------------------------------------------------------------

/**
 *  去掉字符串前后的空格
 *
 *  @return 返回的是 前后无空格的新的字符串---
 */
-(NSString *)removeStringSpaceBlankEndAndStart;

/**
 *  判断字符串是否是由某字符串开头的
 *
 *  @param start 某字符串
 *
 *  @return 是返回YES 否 返回NO
 */
- (BOOL)isStartWithString:(NSString*)start;

/**
 *  判断字符串是否是由某字符串结尾的
 *
 *  @param end 某字符串
 *
 *  @return 是返回YES 否 返回NO
 */
- (BOOL)isEndWithString:(NSString*)end;

/**
 *  判断字符串是否为空
 *
 *  @param string
 *
 *  @return 不为空则返回YES 否则返回NO
 */
+(BOOL)checkStringIsValided:(NSString*)string;

//-----------------------------------------------------------------------------------
// 字符串操作集合，包括
// 1)是否是手机号
// 2)是否是邮箱
// 3)手机号的类型
// 4)直接拨打电话
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// 安全相关的字符串操作集合，包括
// 1) 3DES加密解密
// 2) Base64编码解码
//-----------------------------------------------------------------------------------

//md5加密
+ (NSString *)encryptPassword:(NSString *)str;

- (NSString *)stringFromMD5;

/*!
 * 使用3DES算法对字符串进行加密，加密后使用base64编码
 * 3DES算法要求密钥是24字节长，这里要求一个24个字符的字符串做为参数
 */
- (NSString *)encryptWithKey:(NSString*)key;

/*!
 * 先解码base64，然后使用3DES算法解密
 * 3DES算法要求密钥是24字节长，这里要求一个24个字符的字符串做为参数
 */
- (NSString *)decryptBase64WithKey:(NSString*)key;

/*!
 * 返回一个字符串的base64编码结果
 */
- (NSString *)base64String;

/*!
 * 返回一个二进制数据的base64编码结果
 */
+ (NSString *)encodeBase64WithData:(NSData *)objData;

/*!
 * 将一个base64编码字符串解码成二进制数据
 */
+ (NSData *)decodeBase64WithString:(NSString *)strBase64;

/*!
 * 为空处理
 */
- (NSString *) safeString;

/**
 *  将NSArray转换为NSString
 *
 *  @param array
 *
 *  @return
 */
+ (NSString *) jsonStringWithArray:(NSArray *)array;

/**
 *  判断是否为空字符串
 *
 *  @return Bool
 */
+ (BOOL)isBlankString:(id)string;

/** 限制苹果系统输入法  禁止输入表情
 *  -(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text 中调用
 *
 *  @return
 */
- (BOOL)stringUITextInputMode;

/**
 *  是否包含表情
 *
 *  @return
 */
- (BOOL) containEmoji;

/**
 *  表情检测  与 containEmoji 联合使用
 *
 *  @param string
 *
 *  @return 
 */
- (BOOL) stringContainsEmoji;
@end
