//
//  NSUserDefaults+Template.m
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "NSUserDefaults+Template.h"

@implementation NSUserDefaults (Template)

- (void)synchronizeObject:(nullable id)obj forKey:(nonnull NSString *)key
{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:obj forKey:key];
    [userDefault synchronize];
}

@end
