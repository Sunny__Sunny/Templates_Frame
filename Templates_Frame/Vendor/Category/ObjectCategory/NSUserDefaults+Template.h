//
//  NSUserDefaults+Template.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Template)

/**
 *  直接synchronize
 *
 *  @param obj 对象
 *  @param key 键
 */
- (void) synchronizeObject:(nullable id)obj forKey:(nonnull NSString *)key;

@end
