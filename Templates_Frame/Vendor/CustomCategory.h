//
//  CustomCategory.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#ifndef CustomCategory_h
#define CustomCategory_h

/**
 *  Foundation + Catagory
 */
#import "NSDate+Template.h"
#import "NSMutableDictionary+Template.h"
#import "NSString+Template.h"
#import "NSTimer+Template.h"
#import "NSUserDefaults+Template.h"


/**
 *  UIKit + Catagory
 */
#import "UIImage+RoundCorner.h"
#import "UIImageView+NetImage.h"
#import "UIApplication+Template.h"
#import "UIButton+Template.h"
#import "MBProgressHUD+Template.h"
#import "UIButton+Template.h"
#import "UIApplication+Template.h"
#import "UIView+Template.h"
#import "UITextField+Template.h"
#import "UIViewController+TabbarHiddenWhenPush.h"

#endif /* CustomCategory_h */
