//
//  AppDelegate+UMeng.h
//
//
//  Created by Sunny on 16/7/8.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (UMeng)
/**
 *  初始化友盟
 */
- (void)umengApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

/**
 *  友盟统计统一调用方法
 */

// eg:
/*
#pragma mark - **************** 视图将要展示
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [AppDelegate beginLogPageView:[MainViewController class]];
}
#pragma mark - **************** 视图将要消失
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [AppDelegate endLogPageView:[MainViewController class]];
}
 
*/
// 开始记录页面停留的时间
+ (void)beginLogPageView:(__unsafe_unretained Class)pageView;
// 结束记录页面停留的时间
+ (void)endLogPageView:(__unsafe_unretained Class)pageView;
// 事件统计统计发生的次数
+ (void)event:(NSString *)eventID;
// 统计统计点击行为各属性被触发的次数
+ (void)event:(NSString *)eventID attributes:(NSDictionary *)attributes;

@end
