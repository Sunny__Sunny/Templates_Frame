
//
//  NoDataView.m
//  zoneTry
//
//  Created by Zonetry on 16/8/9.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "RequestNoDataView.h"

@implementation RequestNoDataView

+(id)noDataViewWithFrame:(CGRect)frame
{
    return [[RequestNoDataView alloc] initWithFrame:frame];
}

-(id)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor clearColor];
        UIImage *backgroundImage=[UIImage imageNamed:@"blank"];
        UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-backgroundImage.size.width)/2.0f, (self.frame.size.height-backgroundImage.size.height)/2.0f, backgroundImage.size.width, backgroundImage.size.height)];
        imageView.image=backgroundImage;
        [self addSubview:imageView];
    }
    return self;
}

@end
