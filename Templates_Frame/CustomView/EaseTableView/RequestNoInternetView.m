//
//  NoInternetView.m
//  zoneTry
//
//  Created by Zonetry on 16/8/9.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "RequestNoInternetView.h"

/******等比例放大,以iphone6尺寸为基础*******/
#define RationEnlarge(data) data*[UIScreen mainScreen].bounds.size.width/375.0

@implementation RequestNoInternetView

/******类方法创建对象*******/
+(id)noInternetViewWithFrame:(CGRect)frame
{
    return [[RequestNoInternetView alloc] initWithFrame:frame];
}
-(id)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor clearColor];
        UIImage *noInternetImage=[UIImage imageNamed:@"no_network"];
        UIImageView *noInternetImageView=[[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-noInternetImage.size.width)/2.0f, (self.frame.size.height-110-noInternetImage.size.height)/2.0f, noInternetImage.size.width, noInternetImage.size.height)];
        noInternetImageView.image=noInternetImage;
        [self addSubview:noInternetImageView];
        
        UIButton *refreshBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        refreshBtn.frame=CGRectMake((self.frame.size.width-RationEnlarge(224))/2.0f, CGRectGetMaxY(noInternetImageView.frame)+66, RationEnlarge(224), 44);
        refreshBtn.layer.borderWidth=1.0f;
        refreshBtn.layer.borderColor=[UIColor colorWithHue:238 saturation:180 brightness:0 alpha:1].CGColor;
        refreshBtn.layer.cornerRadius=8;
        refreshBtn.layer.masksToBounds=YES;
        refreshBtn.backgroundColor=[UIColor clearColor];
        refreshBtn.titleLabel.font=[UIFont systemFontOfSize:16];
        [refreshBtn setTitleColor:[UIColor colorWithHue:0 saturation:0 brightness:0 alpha:1] forState:UIControlStateNormal];
        [refreshBtn setTitle:@"刷新试试" forState:UIControlStateNormal];
        [refreshBtn addTarget:self action:@selector(refreshBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:refreshBtn];
    }
    return self;
}

-(void)refreshBtnClicked:(UIButton *)sender
{
    if (self.refreshBtnClicked) {
        self.refreshBtnClicked(sender);
    }
}

@end
