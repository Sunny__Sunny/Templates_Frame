//
//  NoDataView.h
//  zoneTry
//
//  Created by Zonetry on 16/8/9.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestNoDataView : UIView

/******类方法创建对象*******/
+(id)noDataViewWithFrame:(CGRect)frame;

@end
