//
//  NoInternetView.h
//  zoneTry
//
//  Created by Zonetry on 16/8/9.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestNoInternetView : UIView

/******点击刷新按钮的一个回调*******/
@property(nonatomic,copy)void(^refreshBtnClicked)(UIButton *sender);

/******类方法创建对象*******/
+(id)noInternetViewWithFrame:(CGRect)frame;


@end
