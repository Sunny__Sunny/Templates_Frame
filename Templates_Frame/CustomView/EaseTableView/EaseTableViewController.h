//
//  EaseTableViewController.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestNoDataView.h"
#import "RequestNoInternetView.h"

#define KCELLDEFAULTHEIGHT 50

@interface EaseTableViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>
{
    
}

@property (strong, nonatomic) RequestNoDataView *noDataView;         // 没数据空态图
@property (strong, nonatomic) RequestNoInternetView *noInternetView; // 没网空态图
@property (strong, nonatomic) UIView *defaultFooterView;             // 默认footer
@property (strong, nonatomic) UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *dataArray;              // 数据源数组
@property (strong, nonatomic) NSMutableDictionary *dataDictionary;    // 数据源字典

@property (nonatomic, assign) NSInteger page;                         // 页数
@property (nonatomic) BOOL showRefreshHeader;                         // 是否支持下拉刷新
@property (nonatomic) BOOL showRefreshFooter;                         // 是否支持上拉加载
@property (nonatomic,assign) BOOL noMoreDate;                         // 当大于一页的时候调用
@property (nonatomic) BOOL showTableBlankView;                        // 是否显示无数据时默认背景

- (instancetype)initWithStyle:(UITableViewStyle)style;

- (void)tableViewDidTriggerHeaderRefresh;   //下拉刷新事件
- (void)tableViewDidTriggerFooterRefresh;   //上拉加载事件

- (void)tableViewDidFinishTriggerHeader:(BOOL)isHeader reload:(BOOL)reload; // 结束刷新

@end
