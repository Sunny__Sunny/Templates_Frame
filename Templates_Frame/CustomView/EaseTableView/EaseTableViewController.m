//
//  EaseTableViewController.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "EaseTableViewController.h"
#import "MJRefresh.h"
#import "SDWebImageManager.h"

@interface EaseTableViewController ()

@property (nonatomic, readonly) UITableViewStyle style;
@property (nonatomic, strong) MJRefreshBackNormalFooter *normalFooter;
@property (nonatomic, strong) MJRefreshAutoNormalFooter *autoNomalFooter;

@end

@implementation EaseTableViewController

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {
        _style = style;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:self.style];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = self.defaultFooterView;
    _tableView.backgroundColor = KBaseViewColor;
    [self.view addSubview:_tableView];
    
    _page = 0;
    _showRefreshHeader = NO;
    _showRefreshFooter = NO;
    _showTableBlankView = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    SDWebImageManager *mgr = [SDWebImageManager sharedManager];
    
    // 1.取消正在下载的操作
    [mgr cancelAll];
    // 2.清除内存缓存
    [mgr.imageCache clearMemory];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setter

- (void)setShowRefreshHeader:(BOOL)showRefreshHeader
{
    if (_showRefreshHeader != showRefreshHeader) {
        _showRefreshHeader = showRefreshHeader;
        if (_showRefreshHeader) {
            __weak EaseTableViewController *weakSelf = self;
            self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                [weakSelf tableViewDidTriggerHeaderRefresh];
                [weakSelf.tableView.mj_header beginRefreshing];
            }];
        }
        else{
            self.tableView.mj_header = nil;
        }
    }
}

- (void)setShowRefreshFooter:(BOOL)showRefreshFooter
{
    if (_showRefreshFooter != showRefreshFooter) {
        _showRefreshFooter = showRefreshFooter;
        if (_showRefreshFooter) {
            if (!self.tableView.mj_footer) {
                self.tableView.mj_footer = self.normalFooter;
                self.tableView.mj_footer.hidden = NO;
            }else{
                self.tableView.mj_footer = self.normalFooter;
                self.tableView.mj_footer.hidden = NO;
            }
        }else{
            if (self.tableView.mj_footer) {
                self.tableView.mj_footer = self.normalFooter;
                self.tableView.mj_footer.hidden = YES;
            }
        }
    }
}

- (void)setNoMoreDate:(BOOL)noMoreDate{
    if (noMoreDate) {
        self.tableView.mj_footer = self.autoNomalFooter;
        self.tableView.mj_footer.hidden = NO;
    }else{
        if (self.tableView.mj_footer) {
            self.tableView.mj_footer = self.autoNomalFooter;
            self.tableView.mj_footer.hidden = YES;
        }
    }
}

- (MJRefreshBackNormalFooter *)normalFooter{

    if (!_normalFooter) {
        __weak EaseTableViewController *weakSelf = self;
        _normalFooter = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf tableViewDidTriggerFooterRefresh];
            [weakSelf.tableView.mj_footer beginRefreshing];
        }];
    }
    return _normalFooter;
}

- (MJRefreshAutoNormalFooter *)autoNomalFooter{

    if (!_autoNomalFooter) {
        _autoNomalFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }];
    }
    return _autoNomalFooter;
}

- (RequestNoInternetView *)noInternetView
{
    if (!_noInternetView) {
        _noInternetView=[RequestNoInternetView noInternetViewWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 64)];
    }
    return _noInternetView;
}

- (RequestNoDataView *)noDataView
{
    if (!_noDataView) {
        _noDataView=[RequestNoDataView noDataViewWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 64 - 44)];
    }
    return _noDataView;
}

- (void)setShowTableBlankView:(BOOL)showTableBlankView
{
    if (_showTableBlankView != showTableBlankView) {
        _showTableBlankView = showTableBlankView;
    }
}

#pragma mark - getter

- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }

    return _dataArray;
}

- (NSMutableDictionary *)dataDictionary
{
    if (_dataDictionary == nil) {
        _dataDictionary = [NSMutableDictionary dictionary];
    }
    
    return _dataDictionary;
}

- (UIView *)defaultFooterView
{
    if (_defaultFooterView == nil) {
        _defaultFooterView = [[UIView alloc] init];
    }
    
    return _defaultFooterView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return KCELLDEFAULTHEIGHT;
}

#pragma mark - public refresh

- (void)autoTriggerHeaderRefresh
{
    if (self.showRefreshHeader) {
        [self tableViewDidTriggerHeaderRefresh];
    }
}

/**
 *  下拉刷新事件
 */
- (void)tableViewDidTriggerHeaderRefresh
{
    
}

/**
 *  上拉加载事件
 */
- (void)tableViewDidTriggerFooterRefresh
{
    
}

- (void)tableViewDidFinishTriggerHeader:(BOOL)isHeader reload:(BOOL)reload
{
    __weak EaseTableViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (reload) {
            [weakSelf.tableView reloadData];
        }
        
        if (isHeader) {
            [weakSelf.tableView.mj_header endRefreshing];
        }
        else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
    });
}


@end
