//
//  CustomUIActionSheet.m
//  zoneTry
//
//  Created by Sunny on 16/7/22.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUIActionSheet : UIView

@property(assign)BOOL cannotDissmissSelf;


-(void) setActionView:(UIView *) view;

-(void) showInView:(UIView *) parentView;

-(void) dissmiss;


@end
