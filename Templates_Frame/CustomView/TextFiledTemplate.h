//
//  TextFiledTemplate.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFiledTemplate : UITextField
{
    BOOL isEnablePadding;
    float paddingLeft;
    float paddingRight;
    float paddingTop;
    float paddingBottom;
}

/**
 *  给textField设置边距
 *
 *  @param enable
 *  @param top
 *  @param right
 *  @param bottom
 *  @param left
 */
- (void)setPadding:(BOOL)enable top:(float)top right:(float)right bottom:(float)bottom left:(float)left;

@end
