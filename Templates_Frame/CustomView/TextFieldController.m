//
//  TextFieldController.m
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "TextFieldController.h"

static NSInteger maxLenth = 50;

@interface TextFieldController ()<UITextFieldDelegate>

{
    BOOL isHaveDian;
}

@property(nonatomic,strong) TextFiledTemplate *textField;
@property(nonatomic,copy) NSString *prevText;

@end

@implementation TextFieldController

#pragma mark ViewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBaseParameter];
    [self setUpUI];
}

#pragma mark Private Method

-(void)setBaseParameter
{
    self.isBackButton=YES;
    [self setRightButtomWithImage:nil orTitle:@"保存"];
}

-(void)setUpUI
{
    [self.view addSubview:self.textField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Get Method

-(TextFiledTemplate *)textField
{
    if (!_textField) {
        _textField = [[TextFiledTemplate alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, 44)];
        _textField.placeholder = self.placeHolder;
        _textField.text = self.content;
        _textField.delegate = self;
        _textField.keyboardType = self.keyboardType;
        _textField.font = [UIFont systemFontOfSize:14];
        _textField.backgroundColor=[UIColor whiteColor];
        UIImageView *tempView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (_textField.height - 25) /2, 25, 25)];
        tempView.image = ImageNamed(@"loading");
        _textField.leftView = tempView;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [_textField setPadding:YES top:3 right:25 + 10 + 25 bottom:3 left:10 + 25];
        self.maxLength = self.maxLength > 0 ? self.maxLength : maxLenth;
    }
    return _textField;
}

#pragma  mark  --  UITextFieldDelegate

- (void)textFieldDidChange:(UITextField *)textField
{
    
    NSString *text = textField.text;
    if ([text stringContainsEmoji]||[text containEmoji]) {
        NSUInteger location = [textField selectedRange].location - 2;
        textField.text = _prevText;
        if (location > _prevText.length) {
            location = _prevText.length;
        }
        [textField setSelectedRange:NSMakeRange(location, 0)];
    } else {
        self.prevText = text;
    }
    if (textField == self.textField) {
        if (textField.text.length > self.maxLength) {
            textField.text = [textField.text substringToIndex:self.maxLength];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //不支持系统表情的输入
    if ([string containEmoji]) {
        return NO;
    }
    if (self.keyboardType == UIKeyboardTypeDecimalPad) {
        if ([textField.text rangeOfString:@"."].location == NSNotFound) {
            isHaveDian = NO;
        }
        if ([string length] > 0) {
            
            unichar single = [string characterAtIndex:0];//当前输入的字符
            if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
                //首字母不能为0和小数点
                if([textField.text length] == 0){
                    if(single == '.') {
                        [MBProgressHUD showWithTitle:@"第一个数字不能为小数点"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }
                    if (single == '0') {
                        [MBProgressHUD showWithTitle:@"第一个数字不能为0"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }
                }

                //输入的字符是否是小数点
                if (single == '.') {
                    if(!isHaveDian)//text中还没有小数点
                    {
                        isHaveDian = YES;
                        return YES;
                        
                    }else{
                        [MBProgressHUD showWithTitle:@"已经输入过小数点了"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }
                }else{
                    if (isHaveDian) {//存在小数点
                        
                        //判断小数点的位数
                        NSRange ran = [textField.text rangeOfString:@"."];
                        if (range.location - ran.location <= 2) {
                            return YES;
                        }else{
                            [MBProgressHUD showWithTitle:@"最多输入两位小数"];
                            return NO;
                        }
                    }else{
                        return YES;
                    }
                }
            }else{//输入的数据格式不正确
                [MBProgressHUD showWithTitle:@"输入的格式不正确"];
                [textField.text stringByReplacingCharactersInRange:range withString:@""];
                return NO;
            }
        }
        else
        {
            return YES;
        }
    }else {
        return YES;
    }
}

#pragma mark Touch Clicked

-(void)rightBtnClick:(UIButton *)button
{
    [self.view endEditing:YES];
    if (![self.textField.text isNotBlank]) {
        [MBProgressHUD showWithTitle:@"输入内容不能为空"];
        return;
    }
    if (self.textFieldControllerDidPreserved) {
        self.textFieldControllerDidPreserved(self.textField.text);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


@end
