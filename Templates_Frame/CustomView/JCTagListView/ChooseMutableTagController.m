//
//  ChooseMutableTagController.m
//  zoneTry
//
//  Created by Sunny on 16/7/15.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "ChooseMutableTagController.h"
#import "JCTagListView.h"


@interface ChooseMutableTagController ()

@property (nonatomic,strong)JCTagListView *tagListView;

@end

@implementation ChooseMutableTagController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.isBackButton = YES;
    [self setRightButtomWithImage:nil orTitle:@"完成"];
    
    _tagListView = [[JCTagListView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - 64)];
    [self.view addSubview:_tagListView];
    
    self.tagListView.canSelectTags = YES;
    self.tagListView.tagStrokeColor = KBaseNavTitleColor;
    self.tagListView.tagBackgroundColor = [UIColor clearColor];
    self.tagListView.tagTextColor = KBaseNavTitleColor;
    self.tagListView.tagSelectedBackgroundColor = KBaseNavTitleColor;
    self.tagListView.tagSelectedColor = KWhiteColor;
    self.tagListView.tagCornerRadius = 5.0f;
    
    if (self.justSelecteOne == YES) {
        self.tagListView.maxSelectTag = 1;
    }
    else {
        self.tagListView.maxSelectTag = 5;
    }
    
    if (self.defTagArray) {
        [self.tagListView.tags addObjectsFromArray:self.defTagArray];
    }else{
        [self.tagListView.tags addObjectsFromArray:@[@"手机游戏", @"互联网金融", @"VR／AR", @"O2O", @"智能硬件", @"社交", @"企业服务", @"医疗健康", @"其他"]];
    }
    if (self.selectTags.count > 0) {
        [self.tagListView.selectedTags addObjectsFromArray:self.selectTags];
    }
    [self.tagListView setCompletionBlockWithSelected:^(NSInteger index) {
        
    }];
}

- (void)rightBtnClick:(UIButton *)button {

    if (self.getSelectTags) {
        self.getSelectTags(self.tagListView.selectedTags);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
