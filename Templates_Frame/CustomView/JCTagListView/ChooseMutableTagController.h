//
//  ChooseMutableTagController.h
//  zoneTry
//
//  Created by Sunny on 16/7/15.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//
#import "BaseViewController.h"

@interface ChooseMutableTagController : BaseViewController

@property (nonatomic,strong) NSArray *defTagArray; // 默认有多少选项

@property (nonatomic,strong) NSArray *selectTags;  // 已经选择的

@property (nonatomic, assign) BOOL justSelecteOne;

/**
 *  选中以后的tag值
 */
@property (nonatomic,copy) void(^getSelectTags)(NSArray *tags);

@end
