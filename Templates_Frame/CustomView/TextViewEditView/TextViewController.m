//
//  TextViewController.m
//  zoneTry
//
//  Created by Zonetry on 16/7/15.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "TextViewController.h"

static NSInteger maxLenth = 200;
static CGFloat textViewMaxHeight = 300;

@interface TextViewController ()<UITextViewDelegate>

@property(nonatomic,strong) UILabel *countLabel; // 显示当前字数的标签
@property(nonatomic,strong) CIPlaceholderTextView *placeHolderView;// 输入域
@property(nonatomic,copy) NSString *prevText;// 上次输入内容用来屏蔽表情符号

@end

@implementation TextViewController

#pragma mark ViewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    self.maxLenght = self.maxLenght > 0 ? self.maxLenght : maxLenth;
    [self setBaseParameter];
    [self setUpUI];
}

#pragma mark Private Method

-(void)setUpUI
{
    [self.view addSubview:self.placeHolderView];
    [self.view addSubview:self.countLabel];
}

-(void)setBaseParameter
{
    [self setRightButtomWithImage:nil orTitle:@"保存"];
    self.isBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma  mark  --  保存按钮点击事件
-(void)rightBtnClick:(UIButton *)button
{
    [self.view endEditing:YES];
    if (![self.placeHolderView.text isNotBlank]) {
        [MBProgressHUD showWithTitle:@"输入内容不能为空"];
        return;
    }
    if (self.textViewControllerDidPreserved) {
        self.textViewControllerDidPreserved(self.placeHolderView.text);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Get Method

-(UILabel *)countLabel
{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.placeHolderView.bottom + 5
                                                              , kScreenWidth - 15, 14)];
        _countLabel.font = [UIFont systemFontOfSize:14];
        _countLabel.textAlignment=NSTextAlignmentRight;
        if (self.content) {
            _countLabel.text = [NSString stringWithFormat:@"%ld/%ld",(unsigned long)self.content.length,(long)self.maxLenght];
        }else {
            _countLabel.text = [NSString stringWithFormat:@"0/%ld",(long)self.maxLenght];
        }
    }
    return _countLabel;
}

-(CIPlaceholderTextView *)placeHolderView
{
    if (!_placeHolderView) {
        _placeHolderView=[[CIPlaceholderTextView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, RationEnlarge(200))];
        _placeHolderView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _placeHolderView.delegate=self;
        _placeHolderView.text = self.content;
        if (!self.placeHolder) {
            _placeHolderView.placeholder=self.placeHolder;
        }
        _placeHolderView.backgroundColor=[UIColor whiteColor];
    }
    return _placeHolderView;
}

#pragma mark UITextViewDelegate

#pragma mark UITextViewDelegate

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return [text stringUITextInputMode];
}

-(void)textViewDidChange:(UITextView *)textView
{
    NSString *text = textView.text;
    if ([text stringContainsEmoji]||[text containEmoji]) {
        NSUInteger location = [textView selectedRange].location - 2;
        textView.text = _prevText;
        if (location > _prevText.length) {
            location = _prevText.length;
        }
        [textView setSelectedRange:NSMakeRange(location, 0)];
    } else {
        self.prevText = text;
    }
    
    NSString *contentStr=textView.text;
    if (contentStr.length>=self.maxLenght) {
        self.countLabel.text=[NSString stringWithFormat:@"%ld/%ld",(long)self.maxLenght,(long)self.maxLenght];
        textView.text=[contentStr substringWithRange:NSMakeRange(0, self.maxLenght)];
        [textView resignFirstResponder];
    }else{
        self.countLabel.text=[NSString stringWithFormat:@"%ld/%ld",(unsigned long)contentStr.length,(long)self.maxLenght];
    }
    //获得textView的初始尺寸
    CGFloat width = CGRectGetWidth(textView.frame);
    CGFloat height = CGRectGetHeight(textView.frame);
    CGSize newSize = [textView sizeThatFits:CGSizeMake(width,MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmax(width, newSize.width), fmax(height, newSize.height < textViewMaxHeight ? newSize.height:textViewMaxHeight));
    textView.frame = newFrame;
    self.countLabel.top = textView.bottom + 5;
}


- (void)backAction:(UIButton *)leftBtn{
    
    if (self.returnConfirm) {
        __weak TextViewController *weak = self;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"确认放弃编辑？" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"放弃" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [weak.navigationController popViewControllerAnimated:YES];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"继续编辑" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
