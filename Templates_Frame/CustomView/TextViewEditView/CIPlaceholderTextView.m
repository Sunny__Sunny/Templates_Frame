//
//  CIPlaceholderTextView.m
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "CIPlaceholderTextView.h"

@interface CIPlaceholderTextView()<UITextViewDelegate>

@property (strong, nonatomic) UILabel *labeltext;
@property (nonatomic,copy) NSString *prevText;

@end

@implementation CIPlaceholderTextView

#pragma mark -
#pragma mark Initialisation

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:self];
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.contentOffset = CGPointMake(0,0);
        self.font = KBaseCellTitleFont;
        [self addSubview:self.labeltext];
        [self addSubview:self.countLabel];
    }
    return self;
}

- (UILabel *)labeltext
{
    if (!_labeltext) {
        _labeltext = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.frame.size.width-10, 20)];
        _labeltext.backgroundColor = [UIColor clearColor];
        _labeltext.textColor = KRGB(185, 185, 185);
        _labeltext.font = KBaseCellTitleFont;
    }
    return _labeltext;
}

#pragma mark -
#pragma mark Setter/Getters

- (void)setPlaceholder:(NSString *)aPlaceholder
{
    self.labeltext.hidden = NO;
    self.labeltext.text = aPlaceholder;
}

- (void)textDidChange:(NSNotification *)notification
{
    id obj = [notification object];
    NSString *text = [obj text];
    
    if (text.length == 0){
        if ([text isEqualToString:@""]) {
            _labeltext.hidden = NO;
        }else{
            _labeltext.hidden = YES;
        }
    }else{
        if (text.length == 1){
            if ([text isEqualToString:@""]) {
                _labeltext.hidden = NO;
            }else{
                _labeltext.hidden = YES;
            }
        }else{
            _labeltext.hidden = YES;
        }
    }
    [self setNeedsDisplay];
}

-(UILabel *)countLabel
{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.frame)-18, kScreenWidth, 14)];
        _countLabel.font = KBaseCellTitleFont;
        _countLabel.textAlignment = NSTextAlignmentRight;
        _countLabel.text = [NSString stringWithFormat:@"0/%ld",(long)self.maxLenght];
        _countLabel.hidden = YES;
    }
    return _countLabel;
}

#pragma mark  --  UITextViewDelegate
-(void)textViewDidChange:(UITextView *)textView
{
    NSString *text = textView.text;
    if ([text stringContainsEmoji] || [text containEmoji]) {
        NSUInteger location = [textView selectedRange].location - 2;
        textView.text = _prevText;
        if (location > _prevText.length) {
            location = _prevText.length;
        }
        [textView setSelectedRange:NSMakeRange(location, 0)];
    } else {
        self.prevText = text;
    }
    
    NSString *contentStr = textView.text;
    if (contentStr.length >= self.maxLenght) {
        self.countLabel.text = [NSString stringWithFormat:@"%ld/%ld",(long)self.maxLenght,(long)self.maxLenght];
        textView.text = [contentStr substringWithRange:NSMakeRange(0, self.maxLenght)];
        [textView resignFirstResponder];
    }else{
        self.countLabel.text = [NSString stringWithFormat:@"%ld/%ld",(unsigned long)contentStr.length,(long)self.maxLenght];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return [text stringUITextInputMode];
}
#pragma mark Private Method

/******取消复制，粘贴*******/
//-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    [UIMenuController sharedMenuController].menuVisible = NO;
//    return NO;
//}

#pragma mark -
#pragma mark Dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.labeltext = nil;
}

@end
