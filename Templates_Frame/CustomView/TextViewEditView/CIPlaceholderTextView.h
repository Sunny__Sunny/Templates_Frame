//
//  CIPlaceholderTextView.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIPlaceholderTextView : UITextView
/**
 *  提示文字
 *
 *  @param placeholder 提示文字
 *
 */
@property(nonatomic,copy)   NSString *placeholder;
/**
 *  显示当前字数的标签,不用传值,方便调用
 */
@property(nonatomic,strong) UILabel *countLabel;
/**
 *  限制输入字数
 */
@property(nonatomic,assign) NSInteger maxLenght;

@end
