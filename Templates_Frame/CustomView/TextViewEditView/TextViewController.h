//
//  TextViewController.h
//  zoneTry
//
//  Created by Zonetry on 16/7/15.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "BaseViewController.h"

@interface TextViewController : BaseViewController
/**
 *  textView边距
 */
@property(nonatomic,assign) UIEdgeInsets contentInsets;
/**
 *  是否返回提示
 */
@property(nonatomic,assign) BOOL returnConfirm;
/**
 *  初始化内容
 */
@property(nonatomic,copy) NSString *content;
/**
 *  提示文字
 */
@property(nonatomic,copy) NSString *placeHolder;
/**
 *  限制输入字数 默认200
 */
@property(nonatomic,assign) NSInteger maxLenght;
/**
 *  确定保存的时候的一个回调
 */
@property(nonatomic,copy) void(^textViewControllerDidPreserved)(NSString *contentString);

@end
