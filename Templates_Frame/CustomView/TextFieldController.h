//
//  TextFieldController.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/7.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldController : BaseViewController

/**
 *  最多文本
 */
@property(nonatomic,assign) NSInteger maxLength;
/**
 *  最少文本
 */
@property(nonatomic,assign) NSInteger minLength;
/**
 *  键盘类型 默认UIKeyboardTypeDefault
 */
@property(nonatomic,assign) UIKeyboardType keyboardType;
/**
 *  初始化内容
 */
@property(nonatomic,copy) NSString *content;
/**
 *  确定保存的时候的一个回调
 */
@property(nonatomic,copy) void(^textFieldControllerDidPreserved)(NSString *contentString);
/**
 *  提示文字
 */
@property(nonatomic,copy) NSString *placeHolder;

@end
