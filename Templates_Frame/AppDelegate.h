//
//  AppDelegate.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/5.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

