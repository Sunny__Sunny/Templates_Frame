//
//  InterfaceHeader.h
//  Uhou_Framework
//
//  Created by Sunny on 16/1/20.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#ifndef InterfaceHeader_h
#define InterfaceHeader_h
#if DEBUG
#define kUrl @"http://www.uhou.com.cn"
#else
#define kUrl @"http://www.uhou.com.cn"
#endif

// 通知
#define KNOTIFICATION_ReloadConnectFromeNet @"KNOTIFICATION_ReloadConnectFromeNet"


#define KNOTIFICATION_AddNewFriend          @"KNOTIFICATION_AddNewFriend"         // 添加好友
#define KNOTIFICATION_DeleteFriend          @"KNOTIFICATION_DeleteFriend"         // 删除好友
#define KNOTIFICATION_ReloadConnectFromeDB  @"KNOTIFICATION_ReloadConnectFromeDB" // 添加是否否进行刷新联系人从数据库
#define KNOTIFICATION_ReloadGroupFromeDB    @"KNOTIFICATION_ReloadGroupFromeDB"   // 添加是否否进行刷新我的圈子从数据库
#define KNOTIFICATION_ReloadGroupFromeNet   @"KNOTIFICATION_ReloadGroupFromeNet"  // 添加是否否进行刷新我的圈子从网络
#define KNOTIFICATION_ChangeConnectCount    @"KNOTIFICATION_ChangeConnectCount"   // 注册更新联系人模块未读消息数

#define KNOTIFICATION_ExitGroup             @"KNOTIFICATION_ExitGroup"            // 退出圈子
#define KNOTIFICATION_addNewGroup           @"KNOTIFICATION_addNewGroup"          // 建立圈子
#define KNOTIFICATION_SomeOneCallMe         @"KNOTIFICATION_SomeOneCallMe"        // 有人@我

#define KNOTIFICATION_ChangeFriendsAlias    @"KNOTIFICATION_ChangeFriendsAlias"   // 修改好友备注

// 环信透传指令
#define KCMD_ADD_FRIEND        @"CMD_ADD_FRIEND"       // 有人添加我为好友
#define KCMD_ADD_GROUP         @"CMD_ADD_GROUP"        // 有人申请加我的圈
#define KCMD_DEL_FRIEND        @"CMD_DEL_FRIEND"       // 有人删除了与我的好友关系
#define KCMD_AgreeFriend_Apply @"CMD_AgreeFriend_Apply"// 同意好友申请

#define KCMD_APPLY_GROUP       @"CMD_APPLY_GROUP"      // 有人同意了我的群组申请
#define KCMD_DEL_GROUP         @"CMD_DEL_GROUP"        // 群主把我踢出圈子
#define KCMD_AgreeGroup_Apply  @"CMD_AgreeGroup_Apply" // 同意圈子申请

#define KCMD_Open_RadBag       @"CMD_Open_RadBag"      // 打开红包
#define KCMD_REVOKE_MESSAGE    @"REVOKE_MESSAGE"       // 撤回消息
#define KCMD_INVITE_GROUP      @"INVITE_GROUP"         // 邀请进圈


/**
 *  登陆注册模块接口
 */
/////////////////////////////////////////////////

#define kLogin_URL               @"/account/login"          // 用户登录
#define kRegist_JudgeSecretURL   @"/account/invitationflag" // 判断是否需要通关密码页面
#define kRegist_ConfirmSecretURL @"/account/invitation"     // 判断通关密语是否正确
#define kRegist_GetCodeURL       @"/account/code"           // 获取验证码
#define kRegist_ConfirmCodeURL   @"/account/validate"       // 判断验证码
#define kRegist_URL              @"/account/register"       // 注册用户
#define kRetrieve_URL            @"/account/forget_password"// 修改密码
/**
 *  首页模块接口
 */
/////////////////////////////////////////////////

#define kHome_refreshTokenURL               @"/users/refreshtoken"                          // 自动刷新Token接口
#define kHome_GetAdListURL                  @"/advs"                                        // 获取广告列表信息接口
#define kHome_GetAdDetail(adv_Id)           [NSString stringWithFormat:@"/advs/%@",adv_Id]  // 广告详情接口
#define kHome_xiaoErPageURL                 @"/users/uid/8"                                 // 小二个人页接口
#define kHome_hasNewADURL                   @"/advs/hasnew"                                 // 是否有新的广告接口
#define kHome_getUserCommentURL(adv_Id)     [NSString stringWithFormat:@"/advs/%@/getcomments",adv_Id]  // 获取广告评论
#define kHome_userPageURL(userUid)          [NSString stringWithFormat:@"/users/uid/%@",userUid]             // 跳转到个人页接口
#define kHome_userSendCommentURL(adv_Id)    [NSString stringWithFormat:@"/advs/%@/sendcomment",adv_Id] // 发送广告评论接口
#define kHome_userDeleteCommentURL(adv_Id)  [NSString stringWithFormat:@"/advs/%@/delcomment",adv_Id]// 删除广告评论接口
/**
 *  消息模块接口
 */
/////////////////////////////////////////////////
#define KMessage_URL                       @""
#define kMessage_GetUserDataWithHx_uidURL  @"/users/hx/"  // 通过环信uid获取用户信息接口
#define kMessage_GetGroupDataWithHx_uidURL @"/groups/hx/" // 通过环信uid获取圈子信息接口
#define kMessage_GetAdminDataWithUidURL    @"/users/uid/8"// 获取小二信息接口
#define kMessage_GetUserDataWithUidURL     @"/users/uid/" // 获取用户信息接口
#define kMessage_ReportURL                 @"/feedback"   // 举报消息接口

#define kMessage_CallUhouURL(groupID,num)  [NSString stringWithFormat:@"/groups/%@/call/?token=%@&num=%ld", groupID, kUserToken, num]   // 呼唤红包接口
#define kMessage_CallUhouPicURL            [NSString stringWithFormat:@"/advs/callpic/?token=%@&type=1",kUserToken]   // 呼唤红包图片接口

/**
 *  圈子相关接口
 */
#define kMessage_TransferGroupLeader(groupID, memberID) [NSString stringWithFormat:@"/groups/%@/transfer/users/%@?token=%@", groupID, memberID, kUserToken]       // 转移圈主接口
#define kMessage_KickGroup(groupID, memberID) [NSString stringWithFormat:@"/groups/%@/delete/users/%@?token=%@", groupID, memberID, kUserToken]                   // 踢出圈子接口
#define kMessage_GetGroupUserList(groupID) [NSString stringWithFormat:@"/groups/%@/members?token=%@", groupID, kUserToken]     // 获取圈子成员信息接口
#define kMessage_GetGroupDetail(groupID) [NSString stringWithFormat:@"/groups/%@?token=%@", groupID, kUserToken]       // 获取圈子信息接口
#define kMessage_UpdateGroupNickname(groupID) [NSString stringWithFormat:@"/groups/%@/update/nick_name?token=%@", groupID, kUserToken]       // 修改圈子名称接口
#define kMessage_ChangeGroupPic(groupID) [NSString stringWithFormat:@"%@/groups/%@/update/photo/?token=%@", kUrl, groupID, kUserToken]       // 修改圈头像
#define kMessage_ChangeGroupAnnounce(groupID) [NSString stringWithFormat:@"/groups/%@/announce",groupID] // 修改圈公告
#define kMessage_UpdateMyGroupAlias(groupID) [NSString stringWithFormat:@"/groups/%@/update/alias?token=%@", groupID, kUserToken]       // 修改我在圈子昵称接口
#define kMessage_ExitGroup(groupID) [NSString stringWithFormat:@"/users/delete/groups/%@?token=%@", groupID, kUserToken]       //  退出圈子接口
#define kMessage_DeleteGroup(groupID) [NSString stringWithFormat:@"/groups/delete/%@?token=%@", groupID, kUserToken]       //  解散圈子接口

#define kMessage_GetGroups [NSString stringWithFormat:@"/users/groups/?token=%@", kUserToken] // 获取所有圈子

/**
 *  联系人模块接口
 */
/////////////////////////////////////////////////
#define KContact_URL @""

// 通过环信id数组获取好友信息
#define KContact_userFriends [NSString stringWithFormat:@"/users/friends_hx/?token=%@", kUserToken]

// 圈主添加好友进圈
#define KContact_GroupInvite(groupId)  [NSString stringWithFormat:@"/groups/%@/invite/?token=%@", groupId, kUserToken]
// 用户推荐好友进圈
#define Kcontact_UserInviteFriends(groupId) [NSString stringWithFormat:@"/users/invite/groups/%@?token=%@", groupId, kUserToken]
// 好友列表url
#define KContact_UserFriends [NSString stringWithFormat:@"/users/friends?token=%@", kUserToken]
// 申请圈子的好友
#define Kcontact_ApplyCircleFriend(groupId) [NSString stringWithFormat:@"/users/friends/groups/%@?token=%@", groupId, kUserToken]
// 删除好友
#define kContact_FriendsAlias @"/users/updatealias/users"                                       
// 好友备注名url
#define kContact_ApplyMessageText @"/users/add/users"                                           
// 申请添加好友的申请
#define KContact_SearchFriend @"/users/search"                                                  
// 查找用户接口
#define Kcontact_DeleteFriend(friendId) [NSString stringWithFormat:@"/users/delete/users/%@?token=%@", friendId, kUserToken]
// 好友申请列表
#define kContact_ApplyFriends @"/users/applyfriend"                                             
// 同意用户申请
#define KContact_AgreeFriendApply(friendId) [NSString stringWithFormat:@"/users/agree/users/%@?token=%@", friendId, kUserToken] 
// 同意圈申请
#define KContact_AgreeGroupApply(groupId,friendId) [NSString stringWithFormat:@"/groups/%@/agree_apply/users/%@?token=%@", groupId, friendId, kUserToken]                      

// 查找用户(通过uid)
#define KContact_SearchFriendWithUid(friendId) [NSString stringWithFormat:@"/users/uid/%@?token=%@", friendId, kUserToken]


// 删除好友申请
#define KContact_DeleteFriendApply [NSString stringWithFormat:@"/users/delete/applyfriend/?token=%@", kUserToken]
// 删除好友申请
#define KContact_DeleteGroupApply [NSString stringWithFormat:@"/users/delete/applygroup/?token=%@", kUserToken]  
// 创建圈子
#define KContact_CreatGroup [NSString stringWithFormat:@"/users/create/groups/?token=%@",kUserToken]             
// 建圈规则
#define KContact_CreatGroupRule @"/groups/rule"                                                                  
// 圈子申请接口
#define kContact_ApplyGroup @"/users/applygroup"                                                                 
// 同意好友申请接口
#define kUrlAgreeTheApply @"/users/agree/users"                                                                  
// 好友申请的好友跳转详细信息页面
#define KContact_ApplyFriendDetail(uid)  [NSString stringWithFormat:@"/users/uid/%@",uid]
// 查找用户(通讯录)
#define KContact_PhoneLink [NSString stringWithFormat:@"/users/searchtel/?token=%@",kUserToken]

/**
 *  我的模块接口
 */
/////////////////////////////////////////////////
#define KMy_URL @""

#define KMy_Feedback           @"/feedback"                      // 用户反馈
#define KMy_UserReceivePage    @"/wallet/receive/"               // 用户收到的红包
#define KMy_UserGivePage       @"/wallet/give/"                  // 发出的红包
#define KMy_GetMoney           @"/users/moneys/"                 // 获取用户钱
#define KMy_Verificationacount @"/cashflows/account/"            // 检测是否绑定微信、支付宝、银行卡
#define KMy_UpdateWX           @"/cashflows/account/delete/wx/"  // 解绑微信
#define KMy_UpdateZFB          @"/cashflows/account/update/zfb/" // 绑定支付宝
#define KMy_UpdateBank         @"/cashflows/account/update/bank/"// 绑定银行卡
#define KMy_cashflows          @"/cashflows/"                    // 提取现金
#define KMy_cashflows_list     [NSString stringWithFormat:@"/cashflows/list/?token=%@",kUserToken] //提现记录

#define kUrlUser @"/users/"                             // 获取用户信息
#define kUrlUser_getUserSpace @"/users/space/"          // 获取相册
#define kUrlUser_password @"/users/password/"           // 修改密码
#define kUrlUser_nikename @"/users/nick_name"           // 修改昵称
#define KMy_changePhoto_background [NSString stringWithFormat:@"/users/photo_background/?token=%@",kUserToken] // 修改背景图片

#define kUrlUser_motto  @"/users/motto"                 // 修改个性签名
#define kUrlUser_gender @"/users/gender"                // 修改性别
#define kUrlUser_photo @"/upload/?token="               // 上传头像

#define kUrlUser_photoSpace [NSString stringWithFormat:@"/users/space/images/?token=%@",kUserToken] // 修改个人空间
#define kUrlUser_photoSpaceSign [NSString stringWithFormat:@"/users/space/content/?token=%@",kUserToken] // 修改空间签名
#define kUrlUser_publishSpace [NSString stringWithFormat:@"/users/event/insert/?token=%@",kUserToken]  //发布动态
#define kUrlUser_deleteSpace  [NSString stringWithFormat:@"/users/event/delete/?token=%@",kUserToken] // 删除动态
#define kUrlUser_selectSpace  [NSString stringWithFormat:@"/users/event/list/?token=%@",kUserToken] // 查询动态

#define kUrlUser_insertTuo [NSString stringWithFormat:@"/users/tuo/insert/?token=%@",kUserToken] // 点妥
#define kUrlUser_deleteTuo [NSString stringWithFormat:@"/users/tuo/delete/?token=%@",kUserToken] // 删除点妥
#define kUrlUser_selectTuo [NSString stringWithFormat:@"users/tuo/list/?token=%@",kUserToken] // 查询点妥

#define kURLUser_changePhoto @"/users/photo/"     // 修改头像
#define kUrlUser_age         @"/users/age"        // 修改年龄
#define kUrlUser_district    @"/users/district"   // 修改地址
#define kUrlUser_school      @"/users/school"     // 修改学校
#define KMy_getCityPlist     @"/static/address.js"// 地区接口


/**
 *  红包模块接口
 */
/////////////////////////////////////////////////
#define kRedPacket_URL                       @""

#define kRedPacket_PersonUrl                 @"/walletperson/show"  // 打开个人红包接口
#define kRedPacket_GroupUrl                  @"/walletgroup/show"   // 打开圈子红包接口
#define kRedPacket_getPersonalRedPacketURL   @"/walletperson/snatch"// 抢个人红包接口
#define kRedPacket_getGroupRedPacketURL      @"/walletgroup/snatch" // 抢圈子红包接口
#define kRedPacket_allUserGettedRedPacketURL @"/wallet/receive"     // 用户收到的所有红包接口
#define kRedPacket_allUserSendedRedPacketURL @"/wallet/give"        // 用户发出的所有红包接口
#define kRedPacket_sendGroupRedPacketURL     @"/walletgroup/create" // 发送圈子红包接口
#define kRedPacket_sendPersonalRedPacketURL  @"/walletperson/create"// 发送个人红包接口

// 创建广告红包
#define kRedPacket_creatAdPacket             [NSString stringWithFormat:@"/walletadv/create/?token=%@", kUserToken]    
// 我发送的广告红包列表
#define kRedPacket_mySendAdPacket            [NSString stringWithFormat:@"/walletadv/sendlist/?token=%@", kUserToken]
// 查找广告红包
#define kRedPacket_searchAdPacket            [NSString stringWithFormat:@"/walletadv/list/?token=%@", kUserToken]
// 我抢到的广告红包列表
#define kRedPacket_myGrabAdPacket            [NSString stringWithFormat:@"/walletadv/receivelist/?token=%@", kUserToken] 



#endif /* InterfaceHeader_h */
