//
//  Test.m
//  Templates_Frame
//
//  Created by Sunny on 16/8/15.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "Test.h"
#import "AutoCoding.h"
@implementation Test

+ (Test *)sharedList
{
    static Test *sharedList = nil;
    if (sharedList == nil)
    {
        //attempt to load saved file
        NSString *path = [[self documentsDirectory] stringByAppendingPathComponent:@"TodoList.plist"];
        sharedList = [Test objectWithContentsOfFile:path];
        
        //if that fails, create a new, empty list
        if (sharedList == nil)
        {
            sharedList = [[Test alloc] init];
        }
    }
    return sharedList;
}

- (id)init
{
    if ((self = [super init]))
    {
        _items = [NSMutableArray array];
    }
    return self;
}

+ (NSString *)documentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

- (void)save
{
    NSString *path = [[[self class] documentsDirectory] stringByAppendingPathComponent:@"TodoList.plist"];
    [self writeToFile:path atomically:YES];
}

@end
