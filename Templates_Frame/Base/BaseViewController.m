//
//  BaseViewController.m
//  Templates_Frame
//
//  Created by Sunny on 16/8/20.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "BaseViewController.h"
#import "TabBarViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController


- (void)viewWillAppear:(BOOL)animated {
    
    [self beginLogPageView];
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar lt_reset];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self endLogPageView];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = KBaseViewColor;
    // Uncomment the following line to preserve selection between presentations.// 延边布局
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout =  UIRectEdgeNone;
    }
    // 设置导航栏标题字体大小
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:18],
       NSForegroundColorAttributeName:kWhiteBackgroundColor}];
}

#pragma mark - 设置导航栏左侧按钮
- (void)setLeftBarButtonItemWithButton:(UIButton *)button andBool:(BOOL)Bool
{
    if (Bool == YES) {
        UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -20;
        self.navigationItem.leftBarButtonItems = @[negativeSpacer, buttonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

#pragma mark - 设置导航栏右侧侧按钮

- (void) setRightButtomWithImage:(NSString *) image orTitle:(NSString *)title{
    
    UIButton *moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    moreBtn.frame = CGRectMake(0, 0, 40, 40);
    [moreBtn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:moreBtn];
    if (image) {
        [moreBtn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -10;
        self.navigationItem.rightBarButtonItems = @[negativeSpacer, rightItem];
    }
    if (title) {
        [moreBtn setTitle:title forState:UIControlStateNormal];
        moreBtn.titleLabel.font = KBaseCellTitleFont;
        [moreBtn setTitleColor:KBaseNavTitleColor forState:UIControlStateNormal];
        [moreBtn setTitleColor:KBaseCellContentColor forState:UIControlStateHighlighted];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
    if (_rightBtn != moreBtn) {
        _rightBtn = moreBtn;
    }
}

- (void)setRightButtomWithTitles:(NSArray *)titles orImages:(NSArray *)images{
    
    NSMutableArray *rightItems = [NSMutableArray array];
    NSInteger count = titles.count>0?titles.count:images.count;
    
    for (int i = 0; i < count; i ++) {
        UIButton *moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn.tag = 10000 + i;
        moreBtn.frame = CGRectMake(0, 0, 40, 40);
        [moreBtn addTarget:self action:@selector(rightBtnClickAtIndex:) forControlEvents:UIControlEventTouchUpInside];
        if (!titles) {
            [moreBtn setImage:ImageNamed([images objectAtIndex:i]) forState:UIControlStateNormal];
            [moreBtn setImage:ImageNamed([images objectAtIndex:i]) forState:UIControlStateSelected];
        }else {
            [moreBtn setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
            moreBtn.titleLabel.font = KBaseCellTitleFont;
            [moreBtn setTitleColor:KBaseNavTitleColor forState:UIControlStateNormal];
            [moreBtn setTitleColor:KBaseCellContentColor forState:UIControlStateHighlighted];
        }
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:moreBtn];
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -10;
        
        [rightItems addObjectsFromArray:@[negativeSpacer,rightItem]];
    }
    self.navigationItem.rightBarButtonItems = rightItems;
}

/**
 *   - 设置返回按钮
 *
 *  @return
 */
- (void)setIsBackButton:(BOOL)isBackButton
{
    if (_isBackButton != isBackButton) {
        _isBackButton = isBackButton;
        // 创建返回按钮
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 44, 44);
        [backButton setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        // 设置导航栏左侧按钮
        [self setLeftBarButtonItemWithButton:backButton andBool:isBackButton];
    }
}

/**
 *  设置关闭按钮
 *
 *  @return
 */
- (void)setIsDismissButton:(BOOL)isDismissButton
{
    if (_isDismissButton != isDismissButton) {
        _isDismissButton = isDismissButton;
        // 创建关闭按钮
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(0, 0, 50, 44);
        [closeButton setImage:[UIImage imageNamed:@"CloseCommentView"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        // 设置导航栏左侧按钮
        [self setLeftBarButtonItemWithButton:closeButton andBool:isDismissButton];
    }
}

/**
 *  设置导航栏标题透明度方法
 *
 *  @return
 */
- (void)setNavTitleBackgroundColorWithAlpha:(CGFloat)alpha
{
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:37.0 / 255 green:31.0 / 255 blue:55.0 / 255 alpha:alpha], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Arial-Bold" size:16], NSFontAttributeName,
      nil]];
}

/**
 *  backAction返回按钮点击事件 pop
 *
 *  @return
 */
- (void)backAction:(UIButton *)button
{
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/**
 *  关闭按钮 dismiss
 *
 *  @param button
 */
- (void)closeAction:(UIButton *)button
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  右侧按钮点击事件
 *
 *  @return
 */
- (void) rightBtnClick:(UIButton *)button{
    
}

/**
 *  右侧按钮点击事件
 *
 *  @return
 */
- (void) rightBtnClickAtIndex:(UIButton *)button{
    
}

/**
 *
 */
- (void) hidenNavBar{
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
}

/**
 *  显示NavgationBar
 */
- (void) showNavBar{
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar lt_setBackgroundColor:kNavBackgroundColor];
}

/**
 *  隐藏／显示状态栏
 */
- (void) statusBarHidden:(BOOL) hiden {

    [[UIApplication sharedApplication]setStatusBarHidden:hiden animated:YES];
}

/**
 *  显示导航栏
 *
 *  @param color 可以设置导航栏颜色
 */
- (void)showNavBar:(UIColor *) color{
    
    self.navigationController.navigationBarHidden = NO;
    if (color) {
        [self.navigationController.navigationBar lt_setBackgroundColor:color];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:kNavBackgroundColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (CGFloat)viewNavHeight{
    return kScreenHeight - 64;
}

- (CGFloat)viewTabHeight {
    return kScreenHeight - 44;
}

- (CGFloat)viewNoneHeight {
    return kScreenHeight;
}

- (CGFloat)viewNavTabHeight {
    return kScreenHeight - 64 - 44;
}
// 开始记录页面停留的时间
-(void)beginLogPageView{
    [MobClick beginLogPageView:NSStringFromClass([self class])];
}
// 结束记录页面停留的时间
-(void)endLogPageView{
    [MobClick endLogPageView:NSStringFromClass([self class])];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
