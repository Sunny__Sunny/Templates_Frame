//
//  BaseModel.h
//  zoneTry
//
//  Created by Sunny on 16/8/8.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject


- (NSString *)description ;

@end
