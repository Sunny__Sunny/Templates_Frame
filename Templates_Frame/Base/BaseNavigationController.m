//
//  ViewController.m
//  Templates_Frame
//
//  Created by Sunny on 16/8/20.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "BaseNavigationController.h"
#import "UINavigationBar+Awesome.h"

// 导航栏背景颜色

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 将导航栏设置为透明
    self.navigationBar.translucent = NO;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navTap)];
    self.navigationBar.userInteractionEnabled = YES;
    [self.navigationBar addGestureRecognizer:recognizer];
    
    // 设置导航栏背景颜色
//    [self.navigationBar lt_setBackgroundColor:[UIColor whiteColor]];
    // 设置导航栏阴影
//    self.navigationBar.shadowImage = [UIImage imageNamed:@"shadow"];
    // Do any additional setup after loading the view, typically from a nib.
}
/**
 *  点击nav 放弃第一响应
 */
- (void) navTap{
    
    [self.topViewController.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
