//
//  BaseModel.m
//  zoneTry
//
//  Created by Sunny on 16/8/8.
//  Copyright © 2016年 ZoneTry. All rights reserved.
//

#import "BaseModel.h"
#import <objc/runtime.h>

@implementation BaseModel

- (NSString *)description
{
    NSMutableString *attrsDesc = [NSMutableString stringWithCapacity:100];
    NSMutableArray *attrName = [NSMutableArray array];
    
    unsigned int outCount = 0;
    objc_property_t *propertys = class_copyPropertyList([self class], &outCount);
    for (int i = 0; i < outCount; i++) {
        objc_property_t property = propertys[i];
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        [attrName addObject:propertyName];
    }
    free(propertys);
        
    for (NSString *attributeName in attrName) {
        
        SEL getSel = NSSelectorFromString(attributeName);
        if ([self respondsToSelector:getSel]) {
            NSMethodSignature *signature = nil;
            signature = [self methodSignatureForSelector:getSel];
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
            [invocation setTarget:self];
            [invocation setSelector:getSel];
            NSObject *__unsafe_unretained valueObj = nil;
            [invocation invoke];
            [invocation getReturnValue:&valueObj];
            if (valueObj) {
                [attrsDesc appendFormat:@" [%@ = %@] ",attributeName,valueObj];
            }else {
                [attrsDesc appendFormat:@" [%@ = nil] ",attributeName];
            }
        }
    }
    NSString *desc = [NSString stringWithFormat:@"%@:{%@}",[self class],attrsDesc];
    return desc;
}

@end
