//
//  BaseViewController.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/20.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <UIKit/UIKit.h>

#define NAVBAR_CHANGE_POINT 50
@interface BaseViewController : UIViewController

@property (nonatomic, assign) BOOL isBackButton;    // 返回按钮
@property (nonatomic, assign) BOOL isDismissButton; // 关闭按钮
@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, readonly) CGFloat viewNavHeight;   // 只有导航栏高度
@property (nonatomic, readonly) CGFloat viewNavTabHeight;// 导航栏和tab高度
@property (nonatomic, readonly) CGFloat viewTabHeight;   // 只有tab高度
@property (nonatomic, readonly) CGFloat viewNoneHeight;  // 都没有高度


/**
 *  设置右侧按钮
 *
 *  @param titles 按钮标题数组
 */
- (void)setRightButtomWithTitles:(NSArray *)titles orImages:(NSArray *)images;

/**
 *  设置右侧按钮
 *
 *  @param image 图片imageString
 */
- (void)setRightButtomWithImage:(NSString *) image orTitle:(NSString *)title;

/**
 *  设置导航栏标题透明度方法
 *
 *  @return
 */
- (void)setNavTitleBackgroundColorWithAlpha:(CGFloat)alpha;

/**
 *  backAction返回按钮点击事件 pop
 *
 *  @return
 */
- (void)backAction:(UIButton *)button;
/**
 *  关闭按钮 dismiss
 *
 *  @param button
 */

- (void)closeAction:(UIButton *)button;

/**
 *  右侧按钮点击事件
 *
 *  @return
 */
- (void)rightBtnClick:(UIButton *)button;
/**
 *  右侧按钮点击事件
 *
 *  @return
 */
- (void)rightBtnClickAtIndex:(UIButton *)button;
/**
 *
 */
- (void)hidenNavBar;

/**
 *  显示NavgationBar
 */
- (void)showNavBar;

/**
 *  显示导航栏
 *
 *  @param color 可以设置导航栏颜色
 */
- (void)showNavBar:(UIColor *) color;

/**
 *  隐藏／显示状态栏
 */
- (void) statusBarHidden:(BOOL) hiden;

/**
 *  开始记录页面停留的时间
 */
-(void)beginLogPageView;

/**
 *  结束记录页面停留的时间
 */
-(void)endLogPageView;

@end
