//
//  SMAFNetworkSessionManager.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMAFNetworkSessionManager : NSObject

/// 返回单例
+(instancetype)sharedInstance;

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager_def;   /* 默认接口 */
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager_adv;   /* 广告评论相关接口 */
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager_base;  /* 用户好友群组相关接口 */
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager_Call;  /* 呼唤红包相关接口 */
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager_Wallet;/* 发送接收查看红包相关接口 */
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager_Static;/* 图片上传相关接口 */

@end
