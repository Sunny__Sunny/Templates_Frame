//
//  CZUploadParam.m
//  传智微博
//
//  Created by apple on 15-3-13.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "SMAFNetworkUpParam.h"

@implementation SMAFNetworkUpParam

- (instancetype)init {

    if (self = [super init]) {
        _picDataArray = [[NSMutableArray alloc] init];
        _videoDataArray = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (SMAFNetworkUpParam *) getUpParamParam {
    
    return [[SMAFNetworkUpParam alloc]init];
}

- (NSString *)picName
{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *time = [NSString stringWithFormat:@"%@",[formatter stringFromDate:currentDate]];
    return [NSString stringWithFormat:@"%@.png",time];
}

- (NSString *)videoName  {
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *time = [NSString stringWithFormat:@"%@",[formatter stringFromDate:currentDate]];
    return [NSString stringWithFormat:@"%@.mov",time];
}

@end
