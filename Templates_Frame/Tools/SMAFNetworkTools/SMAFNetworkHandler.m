//
//  SMAFNetworkHandler.m
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "SMAFNetworkHandler.h"
#import "SMAFNetworkItem.h"
#import "CoreStatus.h"

@implementation SMAFNetworkHandler

+ (SMAFNetworkHandler *)sharedInstance
{
    static SMAFNetworkHandler *handler = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        handler = [[SMAFNetworkHandler alloc] init];
    });
    return handler;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.networkError = NO;
    }
    return self;
}

#pragma makr - 开始监听网络连接

- (SMAFNetworkItem*)requestURL:(NSString *)url
                    networkType:(SMAFNetWorkType)networkType
                         params:(NSMutableDictionary *)params
                     requestId:(NSString *) requestId
                    serverType:(SMNetServer) server
                       delegate:(id)delegate
                        showHUD:(BOOL)showHUD
                   successBlock:(SMAFSuccessBlock)successBlock
                   failureBlock:(SMAFFailureBlock)failureBlock
{
    if (![CoreStatus isNetworkEnable]) {
        [MBProgressHUD showWithTitle:@"网络连接断开,请检查网络!"];
        if (failureBlock) {
            failureBlock(nil,nil,nil);
        }
        return nil;
    }
    /// 如果有一些公共处理，可以写在这里
    NSUInteger hashValue = [delegate hash];
    self.netWorkItem = [[SMAFNetworkItem alloc]initWithtype:networkType
                                                        url:url
                                                     params:params
                                                 serverType:server
                                                   delegate:delegate
                                                  hashValue:hashValue
                                                    showHUD:showHUD
                                               successBlock:successBlock
                                               failureBlock:failureBlock];
    self.netWorkItem.delegate = delegate;
    [self.items addObject:self.netWorkItem];
    return self.netWorkItem;
}

- (void)startMonitoring
{
    __weak SMAFNetworkHandler *weak = self;
    // 1.获得网络监控的管理者
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    // 2.设置网络状态改变后的处理
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 当网络状态改变了, 就会调用这个block
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown: // 未知网络
            {
                ITTDPRINT(@"未知网络");
                weak.networkError = YES;
            }break;
            case AFNetworkReachabilityStatusNotReachable: // 没有网络(断网)
            {
                weak.networkError = YES;
                [MBProgressHUD showActiveMessage:@"网络连接断开,请检查网络!" withBackground:NO];
            }break;
            case AFNetworkReachabilityStatusReachableViaWWAN: // 手机自带网络
            {
                ITTDPRINT(@"手机自带网络");
                weak.networkError = NO;
            }break;
            case AFNetworkReachabilityStatusReachableViaWiFi: // WIFI
            {
                ITTDPRINT(@"WIFI");
                weak.networkError = NO;
            }break;
        }
    }];
    [mgr startMonitoring];
}


/**
 *   懒加载网络请求项的 items 数组
 *
 *   @return 返回一个数组
 */
- (NSMutableArray *)items
{
    if (!_items) {
        _items = [NSMutableArray array];
    }
    return _items;
}
/**
 *   取消所有正在执行的网络请求项
 */
+ (void)cancelAllNetItems
{
    SMAFNetworkHandler *handler = [SMAFNetworkHandler sharedInstance];
    [handler.items removeAllObjects];
    handler.netWorkItem = nil;
}

- (void)netWorkWillDealloc:(SMAFNetworkItem *)itme
{
    [self.items removeObject:itme];
    self.netWorkItem = nil;
}

@end
