//
//  SMAFNetworkManager.h
//  NetwrokDemo
//
//  Created by Ted Liu on 16/5/4.
//  Copyright © 2016年 Ted Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMAFNetworkDefine.h"

@class SMAFNetworkUpParam;
@class SMAFUploadParam;
/// 请求管理者
@interface SMAFNetworkManager : NSObject

/// 返回单例
+(instancetype)sharedInstance;

#pragma mark - 发送 GET 请求的方法

/**
 *   GET请求通过Block 回调结果
 *
 *   @param url          url
 *   @param paramsDict   paramsDict
 *  @param requestId    请求id
 *  @param server       哪个服务器
 *   @param successBlock  成功的回调
 *   @param failureBlock  失败的回调
 *   @param showHUD      是否加载进度指示器
 */
+ (void)getRequstWithURL:(NSString*)url
                  params:(NSDictionary*)paramsDict
               requestId:(NSString *) requestId
              serverType:(SMNetServer) server
            successBlock:(SMAFSuccessBlock)successBlock
            failureBlock:(SMAFFailureBlock)failureBlock
                 showHUD:(BOOL)showHUD;
/**
 *   GET请求通过代理回调
 *
 *   @param url         url
 *   @param paramsDict  请求的参数
 *  @param requestId    请求id
 *  @param server       哪个服务器
 *   @param delegate    delegate
 *   @param showHUD    是否转圈圈
 */
+ (void)getRequstWithURL:(NSString*)url
                  params:(NSDictionary*)paramsDict
               requestId:(NSString *) requestId
              serverType:(SMNetServer) server
                delegate:(id<SMAFNetworkDelegate>)delegate
                 showHUD:(BOOL)showHUD;

#pragma mark - 发送 POST 请求的方法
/**
 *   通过 Block回调结果
 *
 *   @param url           url
 *   @param paramsDict    请求的参数字典
 *  @param requestId    请求id
 *  @param server       哪个服务器
 *   @param successBlock  成功的回调
 *   @param failureBlock  失败的回调
 *   @param showHUD       是否加载进度指示器
 */
+ (void)postReqeustWithURL:(NSString*)url
                    params:(NSDictionary*)paramsDict
                 requestId:(NSString *) requestId
                serverType:(SMNetServer) server
              successBlock:(SMAFSuccessBlock)successBlock
              failureBlock:(SMAFFailureBlock)failureBlock
                   showHUD:(BOOL)showHUD;
/**
 *   post请求通过代理回调
 *
 *   @param url         url
 *   @param paramsDict  请求的参数
 *  @param requestId    请求id
 *  @param server       哪个服务器
 *   @param delegate    delegate
 *   @param showHUD    是否转圈圈
 */
+ (void)postReqeustWithURL:(NSString*)url
                    params:(NSDictionary*)paramsDict
                 requestId:(NSString *) requestId
                serverType:(SMNetServer) server
                  delegate:(id<SMAFNetworkDelegate>)delegate
                   showHUD:(BOOL)showHUD;
#pragma mark - 发送 PATCH 请求的方法
/**
 *   通过 Block回调结果
 *
 *   @param url           url
 *   @param paramsDict    请求的参数字典
 *   @param requestId    请求id
 *   @param server       哪个服务器
 *   @param successBlock  成功的回调
 *   @param failureBlock  失败的回调
 *   @param showHUD       是否加载进度指示器
 */
+ (void)patchReqeustWithURL:(NSString*)url
                     params:(NSDictionary*)paramsDict
                  requestId:(NSString *) requestId
                 serverType:(SMNetServer) server
               successBlock:(SMAFSuccessBlock)successBlock
               failureBlock:(SMAFFailureBlock)failureBlock
                    showHUD:(BOOL)showHUD;
/**
 *   patch请求通过代理回调
 *
 *   @param url         url
 *   @param paramsDict  请求的参数
 *   @param requestId    请求id
 *   @param server       哪个服务器
 *   @param delegate    delegate
 *   @param showHUD    是否转圈圈
 */
+ (void)patchReqeustWithURL:(NSString*)url
                     params:(NSDictionary*)paramsDict
                  requestId:(NSString *) requestId
                 serverType:(SMNetServer) server
                   delegate:(id<SMAFNetworkDelegate>)delegate
                    showHUD:(BOOL)showHUD;


/**
 *  上传文件
 *
 *  @param url          上传文件的 url 地址
 *  @param paramsDict   参数字典
 *  @param requestId    请求id
 *  @param server       哪个服务器
 *  @param requestId    请求ID
 *  @param successBlock 成功
 *  @param failureBlock 失败
 *  @param uploadParam  上传图片 uploadParm中直接添加UIImage
 上传视频 uploadParm中添加ALAsset
 *  @param showHUD      是否显示圈圈
 */
+ (void)uploadFileWithURL:(NSString *)url
                   params:(NSDictionary *)paramsDict
                requestId:(NSString *) requestId
               serverType:(SMNetServer) server
             successBlock:(SMAFSuccessBlock)successBlock
             failureBlock:(SMAFFailureBlock)failureBlock
              uploadParam:(SMAFNetworkUpParam *)uploadParam
                  showHUD:(BOOL)showHUD;

@end
