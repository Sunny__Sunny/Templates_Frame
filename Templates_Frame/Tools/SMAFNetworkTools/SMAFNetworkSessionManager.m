//
//  SMAFNetworkSessionManager.m
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "SMAFNetworkSessionManager.h"

@implementation SMAFNetworkSessionManager

/// 返回单例
static SMAFNetworkSessionManager *_manager = nil;
+ (instancetype) sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[SMAFNetworkSessionManager alloc ]init];
    });
    return _manager;
}

- (AFHTTPSessionManager *)sessionManager_def {
    if (!_sessionManager_def) {
        _sessionManager_def = [SMAFNetworkSessionManager getSessionManageWithUrl:kUrl];
    }
    return _sessionManager_def;
}

- (AFHTTPSessionManager *)sessionManager_adv {
    if (!_sessionManager_adv) {
        _sessionManager_adv = [SMAFNetworkSessionManager getSessionManageWithUrl:kUrl];
    }
    return _sessionManager_adv;
}

- (AFHTTPSessionManager *)sessionManager_base {
    if (!_sessionManager_base) {
        _sessionManager_base = [SMAFNetworkSessionManager getSessionManageWithUrl:kUrl];
    }
    return _sessionManager_base;
}

- (AFHTTPSessionManager *)sessionManager_Call {
    if (!_sessionManager_Call) {
        _sessionManager_Call = [SMAFNetworkSessionManager getSessionManageWithUrl:kUrl];
    }
    return _sessionManager_Call;
}

- (AFHTTPSessionManager *)sessionManager_Wallet {
    if (!_sessionManager_Wallet) {
        _sessionManager_Wallet = [SMAFNetworkSessionManager getSessionManageWithUrl:kUrl];
    }
    return _sessionManager_Wallet;
}

- (AFHTTPSessionManager *)sessionManager_Static {
    if (!_sessionManager_Static) {
        _sessionManager_Static = [SMAFNetworkSessionManager getSessionManageWithUrl:kUrl];
    }
    return _sessionManager_Static;
}

+ (AFHTTPSessionManager *) getSessionManageWithUrl:(NSString *) url {
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manage = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:url] sessionConfiguration:config];
    manage.responseSerializer = [AFJSONResponseSerializer serializer];
    manage.requestSerializer.timeoutInterval = SMAF_API_TIME_OUT;
    
    //    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [self.requestSerializer setValue:@"5d5ccd2a6a4747be9e3d67eb6c67b101" forHTTPHeaderField:@"appId"];
    //    [self.requestSerializer setValue:@"TPA0PyAHpJqNJhJ7ok+6tMyF8CjpY3coSo2/yoYdymsra9Wo3Y7XoA==" forHTTPHeaderField:@"appKey"];
    
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"application/javascript", @"text/html", @"text/plain", nil];
    return manage;
}

@end
