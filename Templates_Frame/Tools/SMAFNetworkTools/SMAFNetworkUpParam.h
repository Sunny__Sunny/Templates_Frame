//
//  CZUploadParam.h
//  传智微博
//
//  Created by apple on 15-3-13.
//  Copyright (c) 2015年 apple. All rights reserved.
//  图片上传的参数模型

#import <Foundation/Foundation.h>

@interface SMAFNetworkUpParam : NSObject

/**
 *  上传视频文件的二进制数据
 */
@property (nonatomic, strong) NSMutableArray *videoDataArray;
/**
 *  上传图片文件的二进制数据
 */
@property (nonatomic, strong) NSMutableArray *picDataArray;

/**
 *  文件名
 */
- (NSString *)picName ;
- (NSString *)videoName ;

/**
 *  通过工厂方法获取参数
 *
 *  @return
 */
+ (SMAFNetworkUpParam *) getUpParamParam;
@end
