//
//  SMAFNetworkResponse.m
//  Uhou_Framework
//
//  Created by Sunny on 16/1/22.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "SMAFNetworkResponse.h"
#import "JSONKit.h"

@implementation SMAFNetworkResponse

/**
 *  返回的结果字符串
 */
- (NSString *)resultString {

    if (self.resultDic) {
        return [self.resultDic JSONString];
    }else {
        return @"";
    }
}
/**
 *  判断是不是成功
 */
- (BOOL)isSuccess {
    
    if ([self.resultDic isKindOfClass:[NSDictionary class]]) {
        
        switch ([self.resultCode integerValue]) {
            case 200:
            {
                return YES;
            }
                break;
            case 300:
            {
                self.errorCode =  [NSString stringWithFormat:@"%@",self.resultDic[@"error_msg"][@"errorCode"]];
                
                switch ([self.errorCode integerValue]) {
                    case 3000001:
                    {
                        self.errorMessage = @"您的帐号在其他设备登录";
                        
                        // 退出当前账号
//                        [[UserManager shareManager] exitLogin];
//                        // 发送异地登录
//                        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@"2"];
                        
                    } break;
                    case 3000002:
                    {
                        self.errorMessage = @"帐号长时间未登录，请重新登录";
                        
                        // 退出当前账号
//                        [[UserManager shareManager] exitLogin];
//                        
//                        // 发送异地登录
//                        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@"2"];
                        
                    } break;
                    case 3000003:
                    {
                        self.errorMessage = @"密码错误，请重新输入";
                    } break;
                    case 3000004:
                    {
                        self.errorMessage = @"验证码错误，请重新输入";
                    } break;
                    case 3000005:
                    {
                        self.errorMessage = @"通关密语不存在或者失效了(┯_┯)";
                    } break;
                    case 3000006:
                    {
                        self.errorMessage = @"帐户已锁定,请稍候重试";
                    } break;
                    case 3001001:
                        self.errorMessage = @"进圈圈失败，再试试吧";
                        break;
                        
                    case 3001002:
                        self.errorMessage = @"找不到此用户";
                        break;
                    case 3001003:
                        self.errorMessage = @"您好像不是圈主哟~";
                        break;
                    case 3001004:
                        self.errorMessage = @"移除成员未成功，请重试";
                        break;
                    case 3001005:
                        self.errorMessage = @"移除成员未成功，请重试";
                        break;
                    case 3001006:
                        self.errorMessage = @"您好像不在这个圈圈";
                        break;
                    case 3001007:
                        self.errorMessage = @"未能成功解散圈圈";
                        break;
                    case 3001008:
                        self.errorMessage = @"未能成功退出圈圈";
                        break;
                    case 3001009:
                        self.errorMessage = @"未能成功退出圈圈";
                        break;
                    case 3001010:
                        self.errorMessage = @"更新圈圈信息失败";
                        break;
                    case 3001011:
                        self.errorMessage = @"找不到这个圈圈";
                        break;
                    case 3001012:
                        self.errorMessage = @"邀请进圈圈失败";
                        break;
                    case 3001013:
//                        self.errorMessage = @"还没有达到活动要求哦~";
                        break;
                    case 3001014:
                        self.errorMessage = @"删除圈圈申请失败";
                        break;
                    case 3001015:
                        self.errorMessage = @"未能成功转让圈主";
                        break;
                    case 3001016:
//                        self.errorMessage = @"还没到活动时间哦~";
                        break;
                    case 3002001:
                        self.errorMessage = @"好友不存在";
                        break;
                    case 3002002:
                        self.errorMessage = @"删除好友失败";
                        break;
                    case 3002003:
                        self.errorMessage = @"添加好友失败";
                        break;
                    case 3002004:
                        self.errorMessage = @"添加好友失败";
                        break;
                    case 3002005:
                        self.errorMessage = @"删除好友申请失败";
                        break;
                    case 3002006:
                        self.errorMessage = @"更新好友备注失败";
                        break;
                    case 3003001:
                        self.errorMessage = @"用户不存在";
                        break;
                    case 3003002:
                        self.errorMessage = @"手机号已存在";
                        break;
                    case 3003003:
                        self.errorMessage = @"油厚号已存在";
                        break;
                    case 3003004:
                        self.errorMessage = @"油厚号格式错误";
                        break;
                    case 3003005:
                        self.errorMessage = @"帐号注册失败，请重试";
                        break;
                    case 3003006:
                        self.errorMessage = @"用户信息更新失败";
                        break;
                    case 3003007:
                        self.errorMessage = @"帐号注册失败，请重试";
                        break;
                    case 3004001:
                        self.errorMessage = @"零钱信息更新失败";
                        break;
                    case 3004002:
                        self.errorMessage = [NSString stringWithFormat:@"%@",self.resultDic[@"error_msg"][@"errorMsg"]];;
                        break;
                    case 3005001:
                        self.errorMessage = @"余额不足，红包发送失败";
                        break;
                    case 3005002:
//                        self.errorMessage = @"抢红包失手了，下次早点哟";
                        break;
                    case 3005005:
//                        self.errorMessage = @"抢红包失手了，下次早点哟";
                        break;
                    case 3006001:
                        self.errorMessage = @"上传头像失败";
                        break;
                    case 3007001:
                        self.errorMessage = @"删除评论失败";
                        break;
                    default:
                        self.errorMessage = [NSString stringWithFormat:@"%@",self.resultDic[@"error_msg"][@"errorMsg"]];
                        break;
                }
                if (self.errorMessage)
                {
                    if (![self.errorMessage isEqualToString:@"用户不存在"]) {
                        [MBProgressHUD showActiveMessage:self.errorMessage withBackground:NO];
                    }
                }
                return NO;
            }
                break;
            default:
            {
                if (!self.errorMessage) {
                    [MBProgressHUD showActiveMessage:self.errorMessage withBackground:NO];
                }
                return NO;
            }
                break;
        }
        
    }else {
        return NO;
    }
}

/**
 *  工厂模式获取类实例
 *
 *  @return
 */

+ (SMAFNetworkResponse *)getResponse {

    return [SMAFNetworkResponse new];
}

@end
