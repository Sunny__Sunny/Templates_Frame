//
//  SMAFNetworkDefine.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#ifndef SMAFNetworkDefine_h
#define SMAFNetworkDefine_h

/**
 *  请求类型
 */
typedef NS_ENUM(NSUInteger, SMNetServer) {
    SMNetServer_Default = 1,     /* 呼唤红包相关接口 */
    SMNetServer_Call,            /* 呼唤红包相关接口 */
    SMNetServer_Wallet,          /* 发送接收查看红包相关接口 */
    SMNetServer_Adv,             /* 广告评论相关接口 */
    SMNetServer_Base,            /* 用户好友群组相关接口 */
    SMNetServer_Static,          /* 图片上传相关接口 */
};


/**
 *  请求类型
 */
typedef NS_ENUM(NSUInteger, SMAFNetWorkType){
    SMAFNetWorkGET = 1,  // GET请求
    SMAFNetWorkPOST,     // POST请求
    SMAFNetWorkPATCH     // PATCH请求
};
/**
 *  网络请求超时的时间
 */
#define SMAF_API_TIME_OUT 10


#if NS_BLOCKS_AVAILABLE
/**
 *  请求开始的回调（下载时用到）
 */
typedef void (^SMAFStartBlock)(void);
/**
 *  请求成功回调
 *
 *  @param returnData 回调block
 */
typedef void (^ SMAFSuccessBlock)(NSURLSessionDataTask * task,SMAFNetworkResponse *response);

/**
 *  请求失败回调
 *
 *  @param error 回调block
 */
typedef void (^ SMAFFailureBlock)(NSURLSessionDataTask * task,SMAFNetworkResponse *response,NSError *error);

#endif


#endif /* SMAFNetworkDefine_h */
