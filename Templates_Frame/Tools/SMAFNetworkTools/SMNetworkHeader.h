//
//  SMNetworkHeader.h
//  Templates_Frame
//
//  Created by Sunny on 16/9/8.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#ifndef SMNetworkHeader_h
#define SMNetworkHeader_h

#import "SMAFNetworkUpParam.h"  // 上传图片参数
#import "SMAFNetworkDelegate.h"
#import "SMAFNetworkResponse.h" // 网络请求返回的数据封装
#import "SMAFNetworkHandler.h"  //
#import "SMAFNetworkManager.h"  // 网络请求管理者

#endif /* SMNetworkHeader_h */
