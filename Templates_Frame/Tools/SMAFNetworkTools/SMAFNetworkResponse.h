//
//  SMAFNetworkResponse.h
//  Uhou_Framework
//
//  Created by Sunny on 16/1/22.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMAFNetworkResponse : NSObject

/**
 *  返回的结果
 */
@property (nonatomic,strong) id resultDic;
/**
 *  返回的结果字符串
 */
@property (nonatomic,strong) NSString *resultString;
/**
 *  返回的状态码
 */
@property (nonatomic, strong) NSString *resultCode;
/**
 *  错误码
 */
@property (nonatomic, strong) NSString *errorCode;
/**
 *  错误信息
 */
@property (nonatomic, strong) NSString *errorMessage;

/**
 *  判断是不是成功
 */
- (BOOL) isSuccess;
/**
 *  工厂模式获取类实例
 *
 *  @return 
 */
+ (SMAFNetworkResponse *) getResponse;
@end
