//
//  ViewController3.m
//  SMCustomeTabBar
//
//  Created by Sunny on 16/8/20.
//  Copyright © 2016年 Sunny. All rights reserved.
//

#import "ViewController3.h"
#import "TextViewController.h"
#import "TextFieldController.h"
#import "ChooseMutableTagController.h"

@interface ViewController3 ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ViewController3

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self hideHud];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.dataArray addObjectsFromArray:@[@"ACActionSheet",@"UUDatePicker",@"TextViewController",@"TextFieldController",@"ChooseMutableTagController"]];
    [self showHudInView:self.view hint:@"吞吞吐吐"];
    // Do any additional setup after loading the view, typically from a nib.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

#pragma mark UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = [self.dataArray objectAtIndex:indexPath.section];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    }else{
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *className = [self.dataArray objectAtIndex:indexPath.section];
    
    if ([className isEqualToString:@"ACActionSheet"]) {
        ACActionSheet *actionSheet = [[ACActionSheet alloc] initWithTitle:nil cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"小视频",@"拍照",@"从手机相册选择"] actionSheetBlock:^(NSInteger buttonIndex) {
            NSLog(@"ACActionSheet block - %ld",buttonIndex);
        }];
        [actionSheet show];
    }else if([className isEqualToString:@"UUDatePicker"]){
        UUDatePicker *datePicker
        = [[UUDatePicker alloc]getPickerWithStyle:UUDateStyle_YearMonthDayHourMinute
                                      didSelected:^(NSString *year,
                                                    NSString *month,
                                                    NSString *day,
                                                    NSString *hour,
                                                    NSString *minute,
                                                    NSString *weekDay) {
                                          NSString *dateString = [NSString stringWithFormat:@"%@-%@-%@ %@:%@",year,month,day,hour,minute];
                                          
                                          ITTDPRINT(@"%@",dateString);
                                          //一个section刷新
//                                          NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:3];
//                                          [tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
                                      }];
        datePicker.minLimitDate = [[NSDate alloc] initWithTimeInterval:60*60*6 sinceDate:[NSDate date]];
//        datePicker.ScrollToDate = [NSDate dateWithString:model.content format:@"yyyy-MM-dd HH:mm"];
        [datePicker showToView:self.view];
    }else{
        [self tabbarHiddenWhenPushMethod:[[NSClassFromString(className) alloc] init]];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY > 0) {
        if (offsetY >= 64) {
            [self setNavigationBarTransformProgress:1];
        } else {
            [self setNavigationBarTransformProgress:(offsetY / 64)];
        }
    } else {
        [self setNavigationBarTransformProgress:0];
        self.navigationController.navigationBar.backIndicatorImage = [UIImage new];
    }
}

- (void)setNavigationBarTransformProgress:(CGFloat)progress
{
    [self.navigationController.navigationBar lt_setTranslationY:(-64 * progress)];
    [self.navigationController.navigationBar lt_setElementsAlpha:(1-progress)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
