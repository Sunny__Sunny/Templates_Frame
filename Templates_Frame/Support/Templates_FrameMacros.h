//
//  Templates_FrameMacros.h
//  Templates_Frame
//
//  Created by Sunny on 16/8/4.
//  Copyright © 2016年 Templates_Frame. All rights reserved.
//
///////////////////////  宏定义  ////////////////////////////////

#ifndef Templates_FrameMacros_h
#define Templates_FrameMacros_h

//================================尺寸===================================

/******等比例放大,以iphone6尺寸为基础*******/
#define RationEnlarge(data) data*[UIScreen mainScreen].bounds.size.width/375.0

//屏幕宽高
#define kScreenCGRect [UIScreen mainScreen].bounds
// 判断当前设备是否是5.5寸屏
#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断当前设备是否是4.7寸屏
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断当前设备是否是4寸屏
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断当前设备是否是3.5寸屏
#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ?( CGSizeEqualToSize(CGSizeMake(320, 480), [[UIScreen mainScreen] currentMode].size) ||CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size)) : NO)

//================================颜色===================================
// 标签栏背景颜色
#define kTabBarBackgroundColor [UIColor colorWithRed:250.0 / 255 green:250.0 / 255 blue:250.0 / 255 alpha:1.0]
// 非纯白背景颜色
#define kWhiteBackgroundColor [UIColor colorWithRed:30.0 / 255 green:30.0 / 255 blue:30.0 / 255 alpha:1.0]

//RGB
#define KRGB(R,G,B) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1.f]
//RGBA
#define KRGBA(R,G,B,A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
//清除背景色
#define CLEARCOLOR [UIColor clearColor]

// 导航栏背景颜色
#define kNavBackgroundColor [UIColor colorWithRed:30.0 / 255 green:30.0 / 255 blue:30.0 / 255 alpha:1.0]
// baseView颜色
#define KBaseViewColor KRGB(238, 238, 238)
// 导航栏 title颜色
#define KBaseNavTitleColor [UIColor colorWithHexString:@"#FFB400"]
// 标签栏 title颜色
#define KBaseTabBarTitleColor [UIColor colorWithHexString:@"#959595"]
// cell title颜色
#define KBaseCellTitleColor [UIColor colorWithHexString:@"#030303"]
// cell content颜色
#define KBaseCellContentColor [UIColor colorWithHexString:@"#777777"]
// cell content颜色
#define KBaseCellRedColor [UIColor colorWithHexString:@"#FE3824"]
// cell 分割线颜色
#define KBaseCellSeparatorColor [UIColor colorWithHexString:@"#C8C7CC"]
// 白色
#define KWhiteColor [UIColor colorWithHexString:@"#FFFFFF"]
//================================字体===================================

// cell title （大字体）大小
#define KBaseCellBigTitleFont [UIFont systemFontOfSize:17]
// cell title （小字体）大小
#define KBaseCellTitleFont [UIFont systemFontOfSize:14]
// cell content 字体大小
#define KBaseCellContentFont [UIFont systemFontOfSize:12]


/**
 *  随机颜色
 *
 *  @param rgbValue arc4random_uniform
 *
 *  @return 随机颜色
 */
#define kRandomColorWithRGB(rgbValue) \
[UIColor colorWithRed:arc4random_uniform(255)/255.0 \
                green:arc4random_uniform(255)/255.0 \
                 blue:arc4random_uniform(255)/255.0 \
                alpha:1]

//=================================调试===================================

//调试
// The general purpose logger. This ignores logging levels.
#ifdef DEBUG
#define ITTDPRINT(xx, ...)  NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define ITTDPRINT(xx, ...)  ((void)0)
#endif

//=================================调试===================================
// #pragma mark - 返回指定的ViewController
#define popToThisContrller(obj,num)  NSArray * viewControllerArray = obj.navigationController.viewControllers;\
UIViewController * controller = [viewControllerArray objectAtIndex:num];\
[obj.navigationController popToViewController:controller animated:YES];
// #pragma mark - 跳转到指定的ViewController
#define pushToDestinationController(viewController,DestinationController)\
DestinationController * controller = [[DestinationController alloc] init];\
[viewController.navigationController pushViewController:controller animated:YES];\

// 快速设置image
#define ImageNamed(_pointer) [UIImage imageNamed:_pointer]

#endif /* ZoneTryCommonMacros_h */
